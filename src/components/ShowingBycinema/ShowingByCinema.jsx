import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Tabs } from 'antd';
import moment from 'moment-timezone';
import 'moment/locale/vi';
import './ShowingByCinema.scss'
import { useNavigate } from 'react-router-dom';
import { setTabs, setTicketBookMovie, setTicketBookSesstion } from '../../Redux/Ticket/ticketBookSlice';
import ModalLogin from '../ModalLogin/ModalLogin';



export default function ShowingByCinema(props) {
    const moviesByCinema = useSelector(movie => movie.filmByCinema?.data.data)
    const dateAllMovies = moviesByCinema?.map(n => n.dates)
    const Dates = dateAllMovies?.find(arr => arr.length === Math.max(...dateAllMovies?.map(arr => arr.length)));


    const [dayData, SetDayData] = useState([])
    const [bundles, SetBundles] = useState()

    const [checkedDay, SetCheckedDay] = useState()
    const [checkedMovies, SetCheckedMovies] = useState()
    const [slug, SetSlug] = useState()
    const userRedux = useSelector((state) => state.user.user);


    const [isModalOpen, setIsModalOpen] = useState(false);
    const [hasNavAddress, setHasNavAddress] = useState("");


    const dispatch = useDispatch()
    const nav = useNavigate()

    const HandleDay = (showDate) => {
        SetDayData(moviesByCinema.filter(n => (n.dates.map(m => m.showDate)).includes(showDate)))
        SetCheckedDay(showDate)
    }
    const HandleMovies = (movies) => {
        SetBundles(movies.dates[0].bundles)
        SetCheckedMovies(movies)
        SetSlug(movies.slug)
    }
    const handleClickSesstion = (bundle, session) => {
        if (!userRedux.Name) {
            setIsModalOpen(true);
            setHasNavAddress(`/booking/${slug}`, { replace: true })
        } else {
            nav(`/booking/${slug}`, { replace: true })

        }
        dispatch(setTabs(2))
        dispatch(setTicketBookMovie({
            name: checkedMovies.movieName,
            age: checkedMovies.age,
            img: checkedMovies.imagePortrait,
            imageLandscape: checkedMovies.imageLandscape
        }))
        let dataChoose = ({
            cine: { name: props.data.name },
            session: session,
            bundle: bundle
        })
        dispatch(setTicketBookSesstion(dataChoose))
    }
    return (
        moviesByCinema && <div className='ShowingByCinema'>
            <div className='dayListCine'>
                <h3>PHIM</h3>
                <ul className='days'>
                    {Dates &&
                        Dates.map((n, i) => {
                            return <li className={checkedDay === n.showDate ? 'Checked day' : 'day'} key={i} onClick={() => HandleDay(n.showDate)}>
                                <p>{n.dayOfWeekLabel}</p>
                                {n.showDate}
                            </li>
                        })
                    }
                </ul>
                <div className='moviesbyday'>
                    {
                        dayData.map((n, i) => {
                            return <div className={checkedMovies === n ? 'Checked boxMovies' : 'boxMovies'} key={i} onClick={() => HandleMovies(n)}>
                                <img src={n.imagePortrait} alt="poster" />
                                <h4 className='moviesbyday_h4'>{n.name}</h4>
                            </div>
                        })
                    }
                </div>
                {bundles && <div className='SessionsByMovie'>
                    <h4>Suất chiếu:</h4>
                    {
                        bundles.map((n, i) => {
                            return <div key={i} className='SesssionBox'>
                                <p>{n.version} - {n.caption === 'sub' ? 'Phụ đề' : 'Lồng tiếng'}</p>
                                {
                                    n.sessions.map((m, j) => {
                                        return <button key={j} onClick={() => handleClickSesstion(n, m)}>
                                            {m.showTime}
                                        </button>
                                    })
                                }
                            </div>

                        })

                    }
                </div>}
                <ModalLogin
                    isModalOpen={isModalOpen}
                    setIsModalOpen={setIsModalOpen}
                    hasNavAddress={hasNavAddress}

                />

            </div>


        </div>
    )
}
