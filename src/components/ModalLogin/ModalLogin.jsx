import { memo, useEffect, useState } from "react";
import { Button, Modal, Tabs } from "antd";
import Login from "./Login/Login";
import Register from "./Register/Register";
import "./ModalLogin.scss";
import { useDispatch, useSelector } from "react-redux";
import { setUserFromSession } from "../../Redux/User/userSlice";

function ModalLogin({ isModalOpen, setIsModalOpen, hasNavAddress = "" }) {
  const [isLogin, setIsLogin] = useState(true);
  const userLocal = JSON.parse(localStorage.getItem("userInfo"));

  const userRedux = useSelector((state) => state.user.user);
  const dispatch = useDispatch();

  // Kiểm tra user đã đăng nhập chưa
  const isUserLogin = userLocal || userRedux.Name;
  useEffect(() => {
    if (userLocal) {
      dispatch(setUserFromSession(userLocal));
    }
  }, []);

  useEffect(() => {
    if (isUserLogin) {
      setIsModalOpen(false);
    }
  }, [isUserLogin]);

  return (
    <Modal
      // title={isLogin ? "Đăng nhập tài khoản" : "Đăng ký tài khoản"}
      open={isModalOpen}
      onCancel={() => {
        setIsModalOpen(false);
        setIsLogin(true);
      }}
      footer={() => {
        isLogin ? <Button>Đăng Ký</Button> : <Button>Đăng Nhập</Button>;
      }}
    >
      <div id="modal-header">
        <img src="/images/icon-login.svg" alt="login images" />
        <h1>{isLogin ? "Đăng nhập tài khoản" : "Đăng ký tài khoản"}</h1>
      </div>

      {isLogin ? (
        <Login setIsModalOpen={setIsModalOpen} hasNavAddress={hasNavAddress} />
      ) : (
        <Register setIsModalOpen={setIsModalOpen} setIsLogin={setIsLogin} />
      )}

      <p style={{ textAlign: "center", marginBottom: "10px" }}>
        {isLogin ? "Bạn chưa có tài khoản?" : "Tôi đã có tài khoản"}
      </p>
      <Button
        // type="primary"
        style={{ width: "100%" }}
        onClick={() => setIsLogin((pre) => !pre)}
      >
        {isLogin ? "Đăng Ký" : "Đăng Nhập"}
      </Button>
    </Modal>
  );
}
export default memo(ModalLogin);
