import { memo } from 'react'
import { Form, Input, Button, message } from 'antd'
import { useDispatch } from 'react-redux';
import { userService } from '../../../services/userAPI';

function Register({ setIsModalOpen, setIsLogin }) {
    const dispatch = useDispatch()
    // dispatch({ type: 'REGISTER', payload: value })
    // setIsModalOpen(pre => !pre)
    const [form] = Form.useForm();

    const HandleRegister = (value) => {
        let user = {
            Email: value.Email,
            Name: value.Name,
            Password: value.Password,
            Role: "user"
        }

        userService.Register(user)
            .then(response => {
                console.log(response)
                // console.log(response.status)
                if (response.status === 200) {
                    message.success("Tạo tài thành công");
                    setIsModalOpen(pre => !pre)
                    setIsLogin(true)
                }
            })
            .catch(error => {
                console.log(error);
                if (error.response.status === 404) {
                    message.error("Tạo tài khoản lỗi! Vui lòng thử lại");
                    form.resetFields();
                }
            }
            )
    }

    return (
        <div className='register flex flex-col gap-4'>
            <Form
                name="Register"
                initialValues={{ remember: true }}
                layout='vertical'
                onFinish={HandleRegister}
                form={form}
            >
                <Form.Item
                    label="Email"
                    name="Email"
                    rules={[
                        { required: true, message: 'Vui lòng nhập email!' },
                        {
                            type: "email",
                            message: "Email không đúng định đạng",
                        },
                    ]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="FullName"
                    name="Name"
                    rules={[{ required: true, message: 'Vui lòng nhập họ tên!' }]}
                >
                    <Input />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="Password"
                    rules={[{ required: true, message: 'Vui lòng nhập password!' }]}
                >
                    <Input.Password />
                </Form.Item>
                <Form.Item
                    label="Confirm Password"
                    name="confirmPassword"
                    rules={[
                        { required: true, message: "Vui lòng nhập password!" },
                        ({ getFieldValue }) => ({
                            validator(_, value) {
                                if (!value || getFieldValue("Password") === value) {
                                    return Promise.resolve();
                                }
                                return Promise.reject("Passwords thì không trùng nhau!");
                            },
                        }),
                    ]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item>
                    <Button type='primary' block htmlType="submit">
                        Đăng ký
                    </Button>
                </Form.Item>
            </Form>
        </div >
    )
}
export default memo(Register)