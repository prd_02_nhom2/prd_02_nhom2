import { memo, useEffect } from "react";
import { Form, Input, Button, Checkbox, message } from "antd";
import { useDispatch, useSelector } from "react-redux";
import "./Login.scss";
import { login, userService } from "../../../services/userAPI";
import customAlert from "../../../utils/customAlert";
import { useNavigate } from "react-router-dom";
function Login({ hasNavAddress }) {
  const isUserLogin = JSON.parse(localStorage.getItem("userInfo"));
  const [form] = Form.useForm();
  const nav = useNavigate();
  const dispatch = useDispatch();
  const userRedux = useSelector((state) => state.user);

  useEffect(() => {
    hasNavAddress && userRedux.user.Name && nav(hasNavAddress);
  }, [hasNavAddress, userRedux.user.Name]);

  const HandleLogin = (value) => {
    dispatch({ type: "LOGIN", payload: value });
  };

  return (
    <Form form={form} onFinish={HandleLogin} layout="vertical">
      <Form.Item
        label="Email"
        name="Email"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Mật Khẩu"
        name="Password"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item>
        <Button
          loading={userRedux.loading}
          type="primary"
          style={{ width: "100%" }}
          htmlType="submit"
        >
          Đăng nhập
        </Button>
      </Form.Item>
    </Form>
  );
}
export default memo(Login);
