import React from "react";
import "./Ticket.scss";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate, useParams } from "react-router-dom";
import { Button } from "antd";
import { SetFinalTicket, setTabs } from "../../Redux/Ticket/ticketBookSlice";
import {
  formatChairList,
  formatDate,
  formatPrice,
} from "../../utils/formatText";

export default function Ticket(props) {
  const nav = useNavigate();
  const { slug } = useParams();
  const dataTicket = useSelector((state) => state.ticketbook);
  const userRedux = useSelector((state) => state.user.user);
  const sessionStorageData = {
    dataMovie: JSON.parse(sessionStorage.getItem("dataMovie")),
    dataSession: JSON.parse(sessionStorage.getItem("dataSession")),
    dataSeat: JSON.parse(sessionStorage.getItem("dataSeat")),
    salary: JSON.parse(sessionStorage.getItem("salary")),
    dataCombo: JSON.parse(sessionStorage.getItem("dataCombo")),
    // user: JSON.parse(sessionStorage.getItem("user")) || userRedux,
    btnTabs: JSON.parse(sessionStorage.getItem("btnTabs")),
    session: JSON.parse(sessionStorage.getItem('session'))

  };
  console.log(sessionStorageData);
  const btnTabs = sessionStorageData.btnTabs;
  const slugBySelectMovie = dataTicket?.dataMovie.slug;
  const isUserSelected = !sessionStorageData?.dataSession?.cine;
  const foodCombo = sessionStorageData.dataCombo
    ? sessionStorageData.dataCombo
    : dataTicket.dataCombo;
  const foodComboPrice = foodCombo
    .map((e) => e.quantity * e.price)
    .reduce((accumulator, currentValue) => {
      return accumulator + currentValue;
    }, 0);
  const dispatch = useDispatch();

  const HandleNext = () => {
    dispatch(setTabs(props.keyName + 1));
    // Nếu đang đứng ở trang /booking thì nav -> booking slug
    if (!slug) {
      nav(`/booking/${slugBySelectMovie}`);
    }
    // Sau khi chọn combo lưu thông tin vé lên reducer
    if (btnTabs === 3) {
      let initialFinalTicket = {
        Price:
          sessionStorageData.salary * sessionStorageData.dataSeat.length +
          foodComboPrice,
        // ShowCode: "cinemaId-sessionId",
        ShowCode: `${sessionStorageData.dataSession.session?.cinemaId}-${sessionStorageData.dataSession.session.id}`,
        Email: userRedux?.Email,
        CinemaName: sessionStorageData.dataSession.cine.name,
        TheaterName: sessionStorageData.session?.screenName,
        FilmName: sessionStorageData.dataMovie.name,
        ImageLandscape: sessionStorageData.dataMovie.imageLandscape,
        ImagePortrait: sessionStorageData.dataMovie.img,
        Combo: JSON.stringify(foodCombo),
        SeatCode: JSON.stringify(sessionStorageData.dataSeat),
        // ShowTime: "2023-01-13T20:30Z",
        ShowTime: formatDate(
          `${sessionStorageData.dataSession.session?.showDate} ${sessionStorageData.session?.showTime}`
        ),
      };
      dispatch(SetFinalTicket(initialFinalTicket));
    }
  };
  const HandleBack = () => {
    if (!slug) {
      nav(`/`);
    }
    if (props.keyName === 2) {
      nav(`/booking`, { replace: true });
      dispatch(setTabs(props.keyName - 1));
    } else {
      dispatch(setTabs(props.keyName - 1));
    }
  };
  return sessionStorageData.dataSession ? (
    <div className="ticket_box">
      <div className="ticket_info">
        <img src={sessionStorageData.dataMovie.img} alt="" />
        <div className="ticket_movie_info">
          <h1>{sessionStorageData.dataMovie.name}</h1>
          <p>
            {sessionStorageData.dataSession.bundle?.version} -{" "}
            {sessionStorageData.dataSession.bundle?.caption === "sub"
              ? "Phụ đề"
              : "Lồng tiếng"}
          </p>
          <span>{sessionStorageData.dataMovie.age}</span>
        </div>
      </div>
      <h2>
        {sessionStorageData.dataSession.cine?.name} -{" "}
        {sessionStorageData.session?.screenName}
      </h2>
      <p>
        Suất:{" "}
        <strong>{sessionStorageData.session?.showTime}</strong> -
        Ngày
        <strong>{sessionStorageData.dataSession.session?.showDate}</strong>
      </p>
      <div className="ticket_possition">
        {sessionStorageData.dataSeat && (
          <div className="ticket_possition_chair">
            <p>
              <strong>{sessionStorageData.dataSeat?.length}x</strong> Ghế Đơn
            </p>
            <p>
              Ghế:{" "}
              <strong>{formatChairList(sessionStorageData.dataSeat)}</strong>
            </p>
          </div>
        )}

        <div className="ticket_possition_price">
          <strong>
            <p>
              {sessionStorageData.dataSeat &&
                formatPrice(
                  sessionStorageData.salary * sessionStorageData.dataSeat.length
                )}{" "}
              ₫
            </p>
          </strong>
        </div>
      </div>

      {/* --=============== Food Combo =============== */}
      {foodCombo.length !== 0 && (
        <div className="food-combo-list">
          {foodCombo.map((combo, i) => {
            return (
              <div className="food-combo-item" key={i}>
                <p className="food-combo-name">
                  <strong>{combo.quantity}x </strong>
                  {combo.name}
                </p>
                <strong className="food-combo-price">
                  {formatPrice(combo.price * combo.quantity)} ₫
                </strong>
              </div>
            );
          })}
        </div>
      )}

      {/* --=============== PRICE: TOTAL =============== */}

      <div className="ticket_allbill">
        <p>
          <strong>Tổng cộng:</strong>
        </p>
        <p style={{ color: "#f58020" }}>
          <strong>
            {sessionStorageData.dataSeat &&
              formatPrice(
                sessionStorageData.salary * sessionStorageData.dataSeat.length +
                  foodComboPrice
              )}{" "}
            ₫
          </strong>
        </p>
      </div>
      {props.hiddenButton || (
        <div className="ticket_button">
          <Button type="default" className="btnBack" onClick={HandleBack}>
            Quay lại
          </Button>
          <Button
            disabled={isUserSelected}
            type="primary"
            className="btnNext"
            onClick={HandleNext}
          >
            Tiếp tục
          </Button>
        </div>
      )}
    </div>
  ) : (
    <div className="ticket_box">
      <div className="ticket_info">
        <img
          src="https://www.galaxycine.vn/_next/static/media/img-blank.bb695736.svg"
          alt=""
        />
      </div>
      <hr />
      <div className="ticket_allbill">
        <p>Tổng cộng:</p>
        <p>0 Đ</p>
      </div>
      <div className="ticket_button">
        <Button
          disabled={true}
          type="default"
          className="btnBack"
          onClick={HandleBack}
        >
          Quay lại
        </Button>
        <Button
          disabled={true}
          type="primary"
          className="btnNext"
          onClick={HandleNext}
        >
          Tiếp tục
        </Button>
      </div>
    </div>
  );
}
