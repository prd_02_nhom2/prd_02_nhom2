import React from 'react';
import { Tabs } from 'antd';

const { TabPane } = Tabs;

const Blog = () => {
  const onChange = (key) => {
    console.log(key);
  };

  return (
    <Tabs defaultActiveKey="1" onChange={onChange}>
      <TabPane tab="Tab 1" key="1">
        <div>
          <h2>Nội dung của Tab 1</h2>
          <img src="link_to_your_image1.jpg" alt="Hình ảnh 1" />
          <p>Đây là nội dung của Tab 1.</p>
        </div>
      </TabPane>
      <TabPane tab="Tab 3" key="3">
        <div>
          <h2>Nội dung của Tab 3</h2>
          <img src="link_to_your_image3.jpg" alt="Hình ảnh 3" />
          <p>Đây là nội dung của Tab 3.</p>
        </div>
      </TabPane>
    </Tabs>
  );
};

export default Blog;
