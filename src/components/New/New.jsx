import React from 'react'
import './new.scss';


export default function New() {
  return (
    <div className='new'>
        <h1>TIN KHUYẾN MÃI</h1>
        <div className='content__items'>
            <div className='content__list--items'>
                <img className='img_content' src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2023%2F10%2F25%2F750_1698224178535.jpg&w=256&q=75'></img>
                <span>Ngày Tri Ân Của Galaxy Cinema - Ngày Thứ Hai ĐẦU TIÊN Mỗi Tháng</span>
            </div>
            <div className='content__list--items'>
                <img className='img_content' src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2022%2F1%2F13%2F450_1642060258003.jpg&w=640&q=75'></img>
                <span>Happy Day</span>
            </div>
            <div className='content__list--items'>
                <img className='img_content' src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2022%2F1%2F13%2F450_1642060258003.jpg&w=640&q=75'></img>
                <span>Happy Day</span>
            </div>
            <div className='content__list--items'>
                <img className='img_content' src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2022%2F1%2F13%2F450_1642060258003.jpg&w=640&q=75'></img>
                <span>Happy Day</span>
            </div>
            <div className='content__list--items'>
                <img className='img_content' src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2023%2F10%2F27%2Fglw-digital-1350x900_1698380396048.jpg&w=640&q=75'></img>
                <span>Galaween: Dare Or Scare - Nhận Ngay Bắp Free!</span>
            </div>
            <div className='content__list--items'>
                <img className="img_content"src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2023%2F1%2F17%2Fbangqltv-2023-digital-1350x900_1673940812370.jpg&w=640&q=75'></img>
                <span>Chào 2023, Đón Mưa Quà Tặng Thành Viên Từ Galaxy Cinema!</span>
            </div>
            <div className='content__list--items'>
                <img className="img_content"src='https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2023%2F1%2F17%2Fbangqltv-2023-digital-1350x900_1673940812370.jpg&w=640&q=75'></img>
                <span>Chào 2023, Đón Mưa Quà Tặng Thành Viên Từ Galaxy Cinema!</span>
            </div>
        </div>
    </div>
  )
}
