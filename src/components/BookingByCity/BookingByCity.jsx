import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./BookingByCity.scss";
import ShowTime from "../../pages/DetailFilm/ShowTime/ShowTime";
import { setTicketBookMovie } from "../../Redux/Ticket/ticketBookSlice";
import { Button, Menu } from "antd";

export default function BookingByCity() {
  const [checked, setChecked] = useState({
    cityId: "",
    movieId: "",
  });
  const allCinemaData = useSelector((cine) => cine.cinema.data.data);
  const allCityData = useSelector((city) => city.city.data.data)?.map(
    (city) => ({ name: city.name, id: city.id })
  );
  const cineShowing = useSelector((cine) => cine.film.data.data?.movieShowing);
  const movieByIdData = useSelector((movie) => movie.moviesById?.data);

  const dispatch = useDispatch();

  const cityFilter = allCityData?.filter((n) =>
    allCinemaData?.map((n) => n.cityId).includes(n.id)
  );
  const [cineShow, SetCineShow] = useState();
  const HandleCity = (id) => {
    SetCineShow(cineShowing);
    setChecked((pre) => ({
      ...pre,
      cityId: id,
    }));
  };
  const HandleMovie = (id, name, age, img, slug, imageLandscape) => {
    dispatch({
      type: "FETCH_MOVIES_BY_ID",
      payload: id,
    });
    dispatch(
      setTicketBookMovie({
        name: name,
        age: age,
        img: img,
        slug: slug,
        imageLandscape: imageLandscape,
      })
    );
  };
  return (
    cityFilter && (
      <div className="bookingByCity">
        <div className="booking_city">
          <h2>Chọn vị trí</h2>
          <div>
            {cityFilter.map((city, i) => {
              console.log(city);
              return (
                <Button
                  type={checked.cityId === city.id ? "primary" : "default"}
                  key={i}
                  onClick={() => HandleCity(city.id)}
                >
                  {city.name}
                </Button>
              );
            })}
          </div>
        </div>
        <div className="booking_movies">
          <h2>Chọn phim</h2>
          <div className="box_movies">
            {cineShow
              ? cineShow.map((cine, i) => {
                  return (
                    <div
                      className={`box_sub ${
                        checked.movieId === cine.id && "is-acctive-class"
                      }`}
                      key={i}
                      onClick={() => {
                        HandleMovie(
                          cine.id,
                          cine.name,
                          cine.age,
                          cine.imagePortrait,
                          cine.slug,
                          cine.imageLandscape
                        );
                        setChecked((pre) => ({
                          ...pre,
                          movieId: cine.id,
                        }));
                      }}
                    >
                      <div className="movie-chooose">
                        <img src={cine.imagePortrait} alt="" />
                        <h2>{cine.name}</h2>
                      </div>
                    </div>
                  );
                })
              : ""}
          </div>
        </div>
        <div className="booking_session">
          <h2>Chọn suất</h2>
          <div>{movieByIdData && <ShowTime data={movieByIdData} />}</div>
        </div>
      </div>
    )
  );
}
