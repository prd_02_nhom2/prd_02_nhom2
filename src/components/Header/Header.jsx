import { memo, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { FaSearch, FaAngleDown, FaBars } from "react-icons/fa";
import { FaClipboardUser, FaListOl } from "react-icons/fa6";
import { TbH1, TbLogout2 } from "react-icons/tb";
import { Button, Drawer, Dropdown, Input, Menu, Space } from "antd";
// import img source
import logo from "../../assets/logo.jpg";
import buyTicket from "../../assets/btn-ticket.jpg";
import ModalLogin from "../ModalLogin/ModalLogin";
import "./header.scss";
import HeaderShowMovie from "./HeaderShowMovie/HeaderShowMovie";
import { logout } from "../../Redux/User/userSlice";
import { headerItems } from "../../Constant/headerItems";
import { setTabs } from "../../Redux/Ticket/ticketBookSlice";
import { useEffect } from "react";

function Header() {
  const userRedux = useSelector((state) => state.user.user);
  const nav = useNavigate();
  const dispatch = useDispatch();
  const [open, setOpen] = useState(false);
  const user = JSON.parse(sessionStorage.getItem("user"));
  const userLocal = JSON.parse(localStorage.getItem("userInfo"));
  const [isModalOpen, setIsModalOpen] = useState(false);
  const isUserSessionStorage = JSON.parse(sessionStorage.getItem("user"));

  //Namttp code rap/giave
  const cinemaAllData = useSelector((cine) => cine.cinema?.data.data);

  //------

  const items = [
    {
      label: (
        <Link className="header-menu-label" to="/tai-khoan/profile">
          <FaClipboardUser />
          Tài Khoản
        </Link>
      ),
      key: "account",
    },
    {
      label: (
        <Link className="header-menu-label" to="/tai-khoan/transaction">
          <FaListOl />
          Lịch Sử
        </Link>
      ),
      key: "transaction",
    },
    {
      label: (
        <div
          className="header-menu-label"
          onClick={() => {
            dispatch(logout());
            nav("/");
          }}
        >
          <TbLogout2 />
          Đăng xuất
        </div>
      ),
      key: "logout",
    },
  ];

  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  const handleBuy = () => {
    dispatch(setTabs(1));
  };

  return (
    <header>
      <div className="header-content">
        {/* LEFT */}
        <div id="header-left">
          <Link to={"/"} className="galaxy-logo">
            <img style={{ width: "115px" }} src={logo} alt="header-logo" />
          </Link>
          <Link to={"/booking"} className="buy-ticket-logo">
            <img
              onClick={handleBuy}
              style={{ width: "115px" }}
              src={buyTicket}
              alt="header-logo"
            />
          </Link>
        </div>

        {/* CENTER */}
        <div id="header-center">
          {/* OLD MENU HEADER */}
          {/* <Menu
                        className="header-center-menu"
                        mode="horizontal"
                        // mode="inline"
                        selectedKeys={[current]}
                        items={headerItems.menuItems}
                    /> */}

          <Menu className="header-center-menu" mode="horizontal">
            {/* --=============== PHIM ĐANG/SẮP CHIẾU =============== */}
            <Menu.SubMenu
              className="modified-menu-label"
              key={"movie"}
              title="Phim"
              icon={<FaAngleDown />}
            >
              <HeaderShowMovie />
            </Menu.SubMenu>

            {/* --=============== GÓC ĐIỆN ẢNH =============== */}
            <Menu.SubMenu
              className="modified-menu-label"
              title="Góc Điện Ảnh"
              key="goc-dien-anh"
              icon={<FaAngleDown />}
            >
              <Menu.Item key="the-loai-phim">
                <Link className="header-menu-label" to={"/dien-anh"}>
                  Thể loại phim
                </Link>
              </Menu.Item>
              <Menu.Item key="dien-vien">
                <Link className="header-menu-label" to={"/dien-vien"}>
                  Diễn viên
                </Link>
              </Menu.Item>
              <Menu.Item key="dao-dien">
                <Link className="header-menu-label" to={"/dao-dien"}>
                  Đạo diễn
                </Link>
              </Menu.Item>
              <Menu.Item key="binh-luan-phim">
                <Link className="header-menu-label" to={"/binh-luan-phim"}>
                  Bình luận phim
                </Link>
              </Menu.Item>
              <Menu.Item key="blog-dien-anh">
                <Link className="header-menu-label" to={"/movie-blog"}>
                  Blog điện ảnh
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            {/* --=============== SỰ KIỆN =============== */}
            <Menu.SubMenu
              className="modified-menu-label"
              title="Sự kiện"
              icon={<FaAngleDown />}
              key="su-kien"
            >
              <Menu.Item key="khuyen-mai">
                <Link className="header-menu-label" to={"/khuyen-mai"}>
                  Ưu Đãi
                </Link>
              </Menu.Item>
              <Menu.Item key="phim-hay">
                <Link className="header-menu-label" to={"/phim-hay"}>
                  Phim hay tháng
                </Link>
              </Menu.Item>
            </Menu.SubMenu>

            {/* --=============== RẠP GIÁ VÉ =============== */}
            <Menu.SubMenu
              className="modified-menu-label"
              title="Rạp/Giá Vé"
              icon={<FaAngleDown />}
              key="rap-gia-ve"
            >
              {cinemaAllData?.map((cine, i) => {
                return (
                  <Menu.Item key={`/rap-gia-ve/${cine.slug}`}>
                    <Link
                      className="header-menu-label"
                      to={`/rap-gia-ve/${cine.slug}`}
                    >
                      {cine.name}
                    </Link>
                  </Menu.Item>
                );
              })}

              {/* <Menu.Item key={'/rap-gia-ve/galaxy-sao'}>
                                <Link className="header-menu-label" to={"/rap-gia-ve/galaxy-sao"}>
                                    Galaxy Sao Hỏa
                                </Link>
                            </Menu.Item> */}
            </Menu.SubMenu>
          </Menu>
        </div>

        {/* LEFT */}
        <div id="header-right">
          <FaSearch className="header-search-icon" />
          {userRedux.Name ? (
            <Dropdown menu={{ items, selectable: true }}>
              <Space className="header-user">
                <img src="/images/user_default.png" alt="" />
                <div className="header-user-info">
                  <div className="top">
                    <img src="/images/icon-star-mini.png" alt="icon-star" />
                    <b>{userRedux.Name ? userRedux.Name : "Null"}</b>
                  </div>
                  <div className="bottom">
                    <img src="/images/icon-gift-mini.png" alt="icon-gift" />
                    <p>0 Stars</p>
                  </div>
                </div>
              </Space>
            </Dropdown>
          ) : (
            <>
              <Button type="text" onClick={() => setIsModalOpen(true)}>
                Đăng nhập
              </Button>
              <div className="header-right-img">
                <img
                  src="https://www.galaxycine.vn/_next/static/media/join-Gstar.24c52de9.svg"
                  alt=""
                />
              </div>
            </>
          )}
          <button className="header-right-mobile-menu" onClick={showDrawer}>
            <FaBars />
          </button>
        </div>

        <Drawer
          id="header-mobile-siderbar"
          title="Galaxy Cinema"
          placement="right"
          onClose={onClose}
          open={open}
        >
          <Input size="large" placeholder="large size" prefix={<FaSearch />} />
          <div className="mobile-siderbar-buy-ticket">
            <img src="/images/btn-ticket.webp" alt="buy-ticket" />
            <img src="/images/join-gstar.svg" alt="" />
          </div>
          <Menu
            className="mobile-siderbar-menu"
            style={{ width: "100%" }}
            mode="inline"
            items={headerItems.menuItems}
          />
        </Drawer>
      </div>
      <ModalLogin isModalOpen={isModalOpen} setIsModalOpen={setIsModalOpen} />
    </header>
  );
}
export default memo(Header);
