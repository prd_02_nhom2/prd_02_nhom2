// HeaderShowMovie.js
import React from 'react';
import { Link } from 'react-router-dom';
import { Image } from 'antd';
import './HeaderShowMovie.scss';
import FilmCommingSoonHeader from '../../../pages/Home/TabFilm/FilmCommingSoon/FilmCommingSoonHeader';
import FilmShowingHeader from '../../../pages/Home/TabFilm/FilmShowing/FilmShowingHeader';

export default function HeaderShowMovie() {
  // Example list of currently showing movies, replace with your actual list
  const currentlyShowingMovies = [
    { imageURL: 'URL_of_currently_showing_movie_1' },
    { imageURL: 'URL_of_currently_showing_movie_2' },
    { imageURL: 'URL_of_currently_showing_movie_3' },
    { imageURL: 'URL_of_currently_showing_movie_4' },
    // Add more movie objects as needed
  ];

  return (
    <div id='HeaderShowMovie'>
      {/* Currently showing movies */}
      <div className="show-movie-section">
        <Link className="show-movie-title" to={"/phim-dang-chieu"}>
          PHIM ĐANG CHIẾU
        </Link>
        <div className='show-movie-content'>
          <FilmShowingHeader movies={currentlyShowingMovies} />
        </div>
      </div>

      {/* Upcoming movies */}
      <div className="show-movie-section">
        <Link className="show-movie-title" to={"/phim-sap-chieu"}>
          PHIM SẮP CHIẾU
        </Link>
        <div className='show-movie-content'>
          <FilmCommingSoonHeader countRender={4} />
        </div>
      </div>
    </div>
  );
}
