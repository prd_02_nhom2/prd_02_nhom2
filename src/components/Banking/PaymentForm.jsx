import React, { useState } from "react";
import { Button, Form, Input } from "antd";
import { useParams } from "react-router-dom";
import InputMask from "react-input-mask";

import "./PaymentForm.scss";
import { useDispatch, useSelector } from "react-redux";
import { ticketService } from "../../services/ticketApi";
import { setTabs } from "../../Redux/Ticket/ticketBookSlice";

const onFinishFailed = (errorInfo) => {
  console.log("Thất bại:", errorInfo);
};

const isNumeric = (value) => {
  return /^[0-9]+$/.test(value);
};

const isAccountNumberValid = (value) => {
  return value.length >= 15 && value.length <= 16;
};

const PaymentForm = ({ selectedBank, form }) => {
  const { bank } = useParams();
  const [cvv, setCvv] = useState("");
  const [accountNumberError, setAccountNumberError] = useState(null);
  const [cvvError, setCvvError] = useState(null);
  const [cardNameError, setCardNameError] = useState(null);
  const dispatch = useDispatch();
  const { finalTicket, loading, ticketSuccess, error } = useSelector(
    (state) => state.ticketbook
  );
  const finalTicket2 = JSON.parse(sessionStorage.getItem('finalTicket'))

  console.log(ticketSuccess);

  ticketSuccess === "Success" && dispatch(setTabs(5));

  const handleCvvInput = (e) => {
    let value = e.target.value;

    // Loại bỏ các ký tự không phải số
    value = value.replace(/[^0-9]/g, "");

    if (value.length > 4) {
      value = value.substr(0, 4);
    }

    // Update the input value
    e.target.value = value;

    // Check if the input is exactly 3 characters
    if (value.length > 2) {
      setCvvError(null);
    } else {
      setCvvError(<span className="custom-error-message">Không hợp lệ</span>);
    }
  };
  const handleCardNumberInput = (e) => {
    let value = e.target.value;

    // Loại bỏ các ký tự không phải số và dấu gạch ngang
    value = value.replace(/[^0-9]/g, "");

    // Đảm bảo giá trị không vượt quá 16 chữ số
    if (value.length > 16) {
      value = value.substr(0, 16);
    }

    // Chèn dấu gạch ngang sau mỗi 4 ký tự
    value = value.replace(/(\d{4})/g, "$1-");

    // Loại bỏ bất kỳ dấu gạch ngang cuối cùng nào
    value = value.replace(/-$/, "");

    // Kiểm tra độ dài và đặt thông báo lỗi tương ứng
    if (value.length < 18) {
      setAccountNumberError(
        <span className="custom-error-message">
          Số tài khoản phải có từ 15 đến 16 chữ số.
        </span>
      );
    } else {
      setAccountNumberError(null);
    }

    // Cập nhật giá trị đầu vào
    e.target.value = value;
  };

  const handleCardNameInput = (e) => {
    let value = e.target.value;

    // Loại bỏ dấu (accents) từ đầu vào
    value = value.normalize("NFD").replace(/[\u0300-\u036f]/g, "");

    // Kiểm tra xem tên không chứa dấu và có đúng một khoảng trống
    const nameParts = value.split(" ");
    if (nameParts.length === 3 && nameParts[1].trim() !== "") {
      setCardNameError(null);
    } else {
      setCardNameError(
        <span className="custom-error-message">
          Tên in trên thẻ không hợp lệ. Vui lòng nhập theo định dạng "Họ Tên".
        </span>
      );
    }

    // Cập nhật giá trị đầu vào
    e.target.value = value;
  };

  const HandlePay = (values) => {
    const troiOiCuuTuii = {
      BankId: selectedBank,
      CardNumber: values.cardNumber.replace(/-/g, ""),
      CardName: values.cardName,
      ExpireDate: values.expirationDate.replace("/", ""),
      CVV: values.cvvCvc,
    };

    // ticketService
    //   .postTicket({
    //     ...troiOiCuuTuii,
    //     ...finalTicket,
    //   })
    //   .then((res) => {
    //     console.log(res);
    //   })
    //   .catch((err) => {
    //     // Handle error
    //     console.log(err);
    //     console.log(err.response);
    //   });

    dispatch({
      type: "CREATE_NEW_TICKET",
      payload: { ...troiOiCuuTuii, ...finalTicket2 },
    });
  };

  return (
    <div className="form-container">
      <Form
        name="basic"
        form={form}
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 26 }}
        style={{ maxWidth: 600 }}
        initialValues={{ remember: true }}
        onFinish={HandlePay}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
      >
        <Form.Item
          name="cardNumber"
          rules={[
            {
              required: true,
              message: (
                <span className="custom-error-message">
                  Vui lòng nhập thông tin cho số thẻ!
                </span>
              ),
            },
          ]}
          help={accountNumberError}
          validateStatus={accountNumberError ? "error" : ""}
        >
          <Input placeholder="Số thẻ" onInput={handleCardNumberInput} />
        </Form.Item>

        <div
          style={{
            display: "flex",
            gridTemplateColumns: "1fr 1fr",
            gridColumnGap: "10px",
          }}
        >
          <Form.Item
            name="expirationDate"
            rules={[
              {
                required: true,
                message: (
                  <span className="custom-error-message">
                    Vui lòng nhập thông tin cho ngày hết hạn!
                  </span>
                ),
              },

              {
                // required: '/^(0[1-9]|1[0-2])\/(0[1-9]|[1-9][0-9])$/'
                pattern: new RegExp(/^(0[1-9]|1[0-2])\/(0[1-9]|[1-9][0-9])$/),
                message: (
                  <span className="custom-error-message">
                    Vui lòng nhập ngày định dạng!
                  </span>
                ),
              },
            ]}
          >
            <InputMask
              mask="99/99"
              maskChar=" "
              placeholder="Ngày hết hạn"
              value={cvv}
            >
              {() => <Input placeholder="Ngày hết hạn" />}
            </InputMask>
          </Form.Item>

          <Form.Item
            name="cvvCvc"
            rules={[
              {
                required: true,
                message: (
                  <span className="custom-error-message">
                    Vui lòng nhập thông tin cho CVV/CVC!
                  </span>
                ),
              },
            ]}
            help={cvvError}
            validateStatus={cvvError ? "error" : ""}
          >
            <Input
              type="password"
              placeholder="CVV/CVC"
              value={cvv}
              onInput={handleCvvInput}
            />
          </Form.Item>
        </div>

        <Form.Item
          name="cardName"
          rules={[
            {
              required: true,
              message: (
                <span className="custom-error-message">
                  Vui lòng nhập thông tin cho tên in trên thẻ!
                </span>
              ),
            },
          ]}
          help={cardNameError}
          validateStatus={cardNameError ? "error" : ""}
        >
          <Input placeholder="Tên in trên thẻ" onInput={handleCardNameInput} />
        </Form.Item>

        <Form.Item
          wrapperCol={{
            offset: 5,
            span: 16,
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            className="ant-btn"
            style={{ width: "100%" }}
            loading={loading}
          >
            Tiếp tục
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};

export default PaymentForm;
