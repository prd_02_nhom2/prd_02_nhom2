import React, { useState } from "react";
import "./Banking.scss";
import PaymentForm from "./PaymentForm";
import { MdPayment } from "react-icons/md";
import { FaCcVisa, FaQrcode } from "react-icons/fa";
import { Form } from "antd";
import { formatPrice } from "../../utils/formatText";
import { useSelector } from "react-redux";

export default function Banking() {
  const [selectedMethod, setSelectedMethod] = useState(null);
  const [selectedBank, setSelectedBank] = useState(null);
  const [bankSelectionMessage, setBankSelectionMessage] = useState("");
  const [showForm, setShowForm] = useState(false);
  const [form] = Form.useForm();


  const finalTicket = useSelector((state) => state.ticketbook.finalTicket)

  const finalTicket2 = JSON.parse(sessionStorage.getItem('finalTicket'))

  const handleMethodSelection = (method) => {
    setSelectedMethod(method);
    // setSelectedBank(null);
    setBankSelectionMessage("");
    setShowForm(false);
  };
  const handleBankLogoClick = (bank) => {
    setSelectedBank(bank === "Agribank" ? 2 : 1);
    setBankSelectionMessage(`Bạn đang thực hiện giao dịch với ${bank}.`);
    setShowForm(true);
    form.resetFields();

    // Loại bỏ lớp 'selected' khỏi tất cả hình ảnh ngân hàng trước đó
    const bankImages = document.querySelectorAll(".bank-logo-container img");
    bankImages.forEach((image) => {
      image.classList.remove("selected");
    });

    // Thêm lớp 'selected' cho hình ảnh ngân hàng được chọn
    const selectedBankImage = document.querySelector(
      `.bank-logo-container img[alt="${bank}"]`
    );
    selectedBankImage.classList.add("selected");
  };

  return (
    <div className="Banking_Infor">
      <div className="order-detail">
        <div className="detail-order-no">
          <span className="break-line">Mã đơn hàng</span>
          <div className="order_code">fp4SZg4z4vZYYEMMzQH9VL</div>
        </div>
        <div className="detail-amount">
          <span className="break-line">Số tiền</span>
          <div className="money">{formatPrice(finalTicket2.Price)} ₫</div>
        </div>
      </div>
      <div className="section-method">
        <ul className="ul-method">
          <li
            className={`nav-method-item nav-qr-tab ${selectedMethod === "local" ? "selected" : ""
              }`}
            onClick={() => handleMethodSelection("local")}
          >
            <MdPayment size={30} />
            <span className="status-bar">Thẻ nội địa</span>
          </li>
          <li
            className={`nav-method-item nav-qr-tab ${selectedMethod === "international" ? "selected" : ""
              }`}
            onClick={() => handleMethodSelection("international")}
          >
            <FaCcVisa size={30} />
            <span className="status-bar">Thẻ quốc tế</span>
          </li>
          <li
            className={`nav-method-item nav-qr-tab ${selectedMethod === "qr" ? "selected" : ""
              }`}
            onClick={() => handleMethodSelection("qr")}
          >
            <FaQrcode size={30} />
            <span className="status-bar">Quét mã QR</span>
          </li>
        </ul>
      </div>
      {showForm && (
        <div className="form-payment-container">
          <div className="payment-form-and-message">
            <PaymentForm selectedBank={selectedBank} form={form} />
            {bankSelectionMessage && (
              <div className="bank-selection-message">
                {bankSelectionMessage}
              </div>
            )}
          </div>
        </div>
      )}
      <div className="payment-method">
        {selectedMethod === "local" && (
          <div className="bank-logo-container">
            <img
              src="https://www.payoo.vn/v2/img/logo_banks/76x42/TP.png"
              alt="TP Bank"
              onClick={() => handleBankLogoClick("TP Bank")}
            />
            <img
              src="https://www.payoo.vn/v2/img/logo_banks/76x42/agribank.png"
              alt="Agribank"
              onClick={() => handleBankLogoClick("Agribank")}
            />
          </div>
        )}
        {selectedMethod === "international" && (
          <div className="international">
            <h3 style={{ textAlign: "center", margin: "30px" }}>
              Coming soon...
            </h3>
          </div>
        )}
        {selectedMethod === "qr" && (
          <div className="qr">
            <h3 style={{ textAlign: "center", margin: "30px" }}>
              Coming soon...
            </h3>
          </div>
        )}
      </div>
    </div>
  );
}
