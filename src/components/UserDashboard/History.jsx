import React from 'react'
import { useSelector } from 'react-redux'
import { format } from 'date-fns';
import { vi } from 'date-fns/locale';
import './History.scss'

export default function History() {
    const dataTicketByEmail = useSelector(n => n.ticketByEmail.data.data)
    console.log(dataTicketByEmail);
    return (
        dataTicketByEmail && <div className='History'>
            <p>Lưu ý: chỉ hiển thị 20 giao dịch gần nhất</p>
            <div className='listBoxTicket'>
                {
                    dataTicketByEmail.map((n, i) => {
                        return <div className='boxTicket'>
                            <div className='imgBox'>
                                <img src={n.ImagePortrait} alt="poster" />
                            </div>
                            <div className='boxContent'>
                                <h4>{n.FilmName}</h4>
                                <p>{JSON.parse(n.SeatCode)}</p>
                                <p>{n.CinemaName} - {n.TheaterName}</p>
                                <p>{format(new Date(n.ShowTime), 'HH:mm - EEEE, d MMMM yyyy ', { locale: vi })}</p>
                            </div>
                        </div>
                    })
                }

            </div>
        </div>
    )
}
