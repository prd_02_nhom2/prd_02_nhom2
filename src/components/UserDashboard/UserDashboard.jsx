import React, { useEffect } from "react";
import { Tabs } from "antd";
import UserForm from "./InforForm"; // Import the UserForm component from the separate file
import InforForm from "./InforForm";
import "./UserDashboard.scss";
import { FaAngleRight } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import userImg from "../../assets/UserImg.png";
import History from "./History";
import { Outlet, useNavigate, useParams } from "react-router-dom";

const items = [
  {
    key: "transaction",
    label: "Lịch Sử Giao dịch",
    children: <History />,
  },
  {
    key: "profile",
    label: "Thông Tin Cá Nhân",
    children: <InforForm />,
  },
  {
    key: "noti",
    label: "Thông Báo",
    children: "Comming Soon...",
  },
  {
    key: "gift",
    label: "Quà Tặng",
    children: "Comming Soon...",
  },
  {
    key: "policy",
    label: "Chính Sách",
    children: "Comming Soon...",
  },
];

const UserDetail = () => <Tabs defaultActiveKey="1" items={items} />;

const UserDashboard = () => {
  const userRedux = useSelector((state) => state.user.user);
  const { slug } = useParams();
  const nav = useNavigate();
  console.log(slug);
  const handleChange = (key) => {
    console.log(key);
    nav(`/tai-khoan/${key}`);
  };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "FETCH_TICKET_EMAIL",
      payload: userRedux.Email,
    });
  });
  return (
    <div className="UserDashboard">
      {/* cot 1 */}
      <div className="User_Infor_list">
        <div className="User_Infor1">
          <div className="img_03">
            <img src="/images/user_default.png" />
          </div>
          <div className="User_Infor_items">
            <div className="Infor_items">
              <img
                className="img_01"
                src="https://www.galaxycine.vn/_next/image/?url=https%3A%2F%2Fcdn.galaxycine.vn%2Fmedia%2F2020%2F5%2F15%2Fs_1589511977688.png&w=32&q=75"
                alt=""
              />
              <p>{userRedux.Name}</p>
            </div>
            <div className="Infor_items">
              <img
                className="img_02"
                src="https://www.galaxycine.vn/_next/image/?url=%2F_next%2Fstatic%2Fmedia%2Ficon-gift.190935e4.png&w=16&q=75"
                alt=""
              />
              <p>0 Stars</p>
            </div>
          </div>
        </div>
        <div className="money_ratting">
          <img src={userImg} alt="" />
        </div>
        <div className="info__support">
          <li className="py-4">
            <h3>
              <strong>HOTLINE hỗ trợ</strong> 19002224 (9:00 - 22:00)
            </h3>
            <p>
              <FaAngleRight />
            </p>
          </li>
          <li className="py-4">
            <h3>
              <strong>Email</strong>hotro@galaxystudio.vn
            </h3>
            <p>
              <FaAngleRight />
            </p>
          </li>
          <li className="py-4">
            <h3>
              <strong>Câu hỏi thường gặp</strong>
            </h3>
            <p>
              <FaAngleRight />
            </p>
          </li>
        </div>
      </div>
      {/* cot 2 */}
      <div className="UserDetail">
        {/* <UserDetail /> */}
        <Tabs activeKey={slug} items={items} onChange={handleChange} />
        {/* <Outlet /> */}
      </div>
    </div>
  );
};

export default UserDashboard;
