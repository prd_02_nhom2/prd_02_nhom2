import React, { useEffect, useState } from "react";
import { Form, Input, Button, Modal, Radio } from "antd";
import "./InforForm.scss"; // Import the SCSS file
import ChangePassword from "./ChangePassword";
import { useSelector } from "react-redux";
import userEvent from "@testing-library/user-event";

const MyFormItemContext = React.createContext([]);

function toArr(str) {
  return Array.isArray(str) ? str : [str];
}

const MyFormItemGroup = ({ prefix, children }) => {
  const prefixPath = React.useContext(MyFormItemContext);
  const concatPath = React.useMemo(
    () => [...prefixPath, ...toArr(prefix)],
    [prefixPath, prefix]
  );
  return (
    <MyFormItemContext.Provider value={concatPath}>
      {children}
    </MyFormItemContext.Provider>
  );
};

const MyFormItem = ({ name, ...props }) => {
  const prefixPath = React.useContext(MyFormItemContext);
  const concatName =
    name !== undefined ? [...prefixPath, ...toArr(name)] : undefined;
  return <Form.Item name={concatName} {...props} />;
};

const InforForm = () => {
  const [form] = Form.useForm();
  const userLogin = useSelector((state) => state.user.user);
  const userChange = useSelector((state) => state.user);

  const onFinish = (value) => {
    console.log(value);
  };

  const [isModalVisible, setIsModalVisible] = useState(false);

  const handleOpenModal = () => {
    setIsModalVisible(true);
  };

  const handleCloseModal = () => {
    setIsModalVisible(false);
  };

  useEffect(() => {
    if (userChange.changePassSuccess === "Success") {
      setIsModalVisible(false);
      console.log("đã thj");
    }
  }, [userChange]);

  return (
    <Form
      form={form}
      layout="vertical"
      name="form_item_path"
      onFinish={onFinish}
    >
      <div className="form_item clearfix">
        <div className="left-myformitem">
          <Form.Item label="Họ và Tên">
            <Input placeholder={userLogin.Name} disabled={true} />
          </Form.Item>

          <Form.Item label="Email">
            <Input placeholder={userLogin.Email} disabled={true} />
          </Form.Item>

          <Form.Item label="Giới tính">
            <Radio.Group disabled={true} defaultValue={"woman"}>
              <Radio value="men"> Nam </Radio>
              <Radio value="woman"> Nữ </Radio>
            </Radio.Group>
          </Form.Item>
        </div>

        <div className="right-myformitem">
          <Form.Item className="Role" label="Role">
            <Input
              placeholder={userLogin.Role === 0 ? "User" : ""}
              disabled={true}
            />
          </Form.Item>
          <Form.Item label="Số điện thoại">
            <Input placeholder="Số điện thoại " disabled={true} />
          </Form.Item>
          <Form.Item name="password" label="Mật khẩu">
            <Input type="password" disabled={true} />
            <span className="changepassword" onClick={handleOpenModal}>
              Thay đổi
            </span>
          </Form.Item>
        </div>
      </div>

      {/* <div className="BtnUpdate">
        <Button type="primary" htmlType="submit">
          Cập nhật
        </Button>
      </div> */}

      <Modal
        title="Thay đổi mật khẩu"
        visible={isModalVisible}
        onCancel={handleCloseModal}
        footer={null} // Remove the footer (OK and Cancel buttons)
        className="modalChange"
      >
        <ChangePassword />
      </Modal>
    </Form>
  );
};

export default InforForm;
