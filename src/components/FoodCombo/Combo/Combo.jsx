import React, { memo, useState } from "react";
import "./Combo.scss";
import { Button, Space, Tooltip } from "antd";
import { PlusOutlined, MinusOutlined } from "@ant-design/icons";
import { useDispatch } from "react-redux";
import {
  addTicketCombo,
  minusTicketCombo,
} from "../../../Redux/Ticket/ticketBookSlice";
import { formatPrice } from "../../../utils/formatText";
function Combo({ combo }) {
  const [quantity, setQuantity] = useState(0);
  const dispatch = useDispatch();

  const HandleMinus = () => {
    if (quantity > 0) {
      setQuantity((pre) => pre - 1);
      let comboTemp = {
        id: combo.id,
        name: combo.description,
        price: combo.displayPrice,
        quantity: quantity - 1,
      };
      dispatch(minusTicketCombo(comboTemp));
    }
  };

  const HandleAdd = () => {
    setQuantity((pre) => pre + 1);
    let comboTemp = {
      id: combo.id,
      name: combo.description,
      price: combo.displayPrice,
      quantity: quantity + 1,
    };
    dispatch(addTicketCombo(comboTemp));
  };

  return (
    <div id="Combo">
      <img src={combo?.imageUrl} alt="FoodCombo IMG" className="combo-img" />
      <div className="combo-content">
        <div className="combo-name">
          <h4> {combo?.description || "iLy Wish Tumbler"}</h4>
        </div>
        <div className="combo-desc">{combo.extendedDescription}</div>
        <div className="combo-price">
          <strong>Giá: {formatPrice(combo.displayPrice)} ₫</strong>

          <Space.Compact>
            <Button
              disabled={!quantity}
              onClick={HandleMinus}
              icon={<MinusOutlined />}
              className="combo-btn"
            />

            <Button
              disabled
              type="default"
              className="combo-btn combo-quantity-special"
            >
              <p>{quantity}</p>
            </Button>

            <Button
              onClick={HandleAdd}
              icon={<PlusOutlined />}
              className="combo-btn"
            />
          </Space.Compact>
        </div>
      </div>
    </div>
  );
}

export default memo(Combo);
