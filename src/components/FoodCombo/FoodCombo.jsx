import React from "react";
import "./FoodCombo.scss";
import { useSelector } from "react-redux";
import Combo from "./Combo/Combo";

export default function FoodCombo() {
  const bookingDetailData = useSelector((n) => n.bookingDetail?.data.data);
  const foodCombo = bookingDetailData.consession[0].concessionItems;

  //--------------------------------------------------------------------------------------//
  //                                        Food Combo                                    //
  //--------------------------------------------------------------------------------------//

  // ---- Food Combo -----------------------------------------------------------------------

  return (
    <div id="food-combo">
      <h3 className="food-combo-title">Chọn Combo</h3>
      <div className="combo-list">
        {foodCombo?.map((combo, i) => {
          return <Combo combo={combo} key={i} />;
        })}
      </div>
      <div className="delete-later"></div>
    </div>
  );
}
