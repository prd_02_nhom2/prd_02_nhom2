import { apiUser } from "../utils/customize-axios";

export const login = async (user) => {
  return await apiUser.post("/Login", user);
};
export const changePassword = async (user) => {
  console.log(user);
  return await apiUser.put("/ChangePassword", user);
};

export const register = async (user) => {
  return await apiUser.post("/user", user);
};

export const userService = {
  Login(user) {
    return apiUser.post("/Login", user);
  },
  Register(user) {
    return apiUser.post("/user", user);
  },
};
