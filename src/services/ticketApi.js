import { api } from "../utils/customize-axios";

export const fetchTicket = async () => {
  return await api.get("/Ticket");
};
export const fetchTicketByShowCode = async (showcode) => {
  return await api.get(`/TicketByShowCode/${showcode}`);
};
export const fetchTicketByEmail = async (email) => {
  return await api.get(`/TicketByEmail/${email}`);
};

export const postTicket = async (ticket) => {
  return await api.post("/Ticket", ticket);
};

export const deleteAllTicket = async () => {
  return await api.delete("/Ticket");
};

export const ticketService = {
  postTicket(ticket) {
    return api.post("/Ticket", ticket);
  },
};
