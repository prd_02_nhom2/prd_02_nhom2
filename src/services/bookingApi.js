import { api } from '../utils/customize-axios'


export const fetchBooking = async () => {
    return await api.get('/booking')
}

export const fetchBookDetail = async () => {
    return await api.get('/booking/detail')
}