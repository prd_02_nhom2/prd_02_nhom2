import { Suspense, lazy, useEffect } from "react";
import {
  BrowserRouter,
  createBrowserRouter,
  Route,
  RouterProvider,
  Routes,
} from "react-router-dom";
import { useDispatch } from "react-redux";
import BookingSlug from "./pages/Booking/BookingSlug";
import Booking from "./pages/Booking/Booking";
import CinemaBySlug from "./pages/CinemaBooking/CinemaBySlug";
import Banking from "./components/Banking/Banking";
import PaymentForm from "./components/Banking/PaymentForm";
import UserDashboard from "./components/UserDashboard/UserDashboard";
import { Tabs } from "antd";
import { userItems } from "./Constant/userItems";

const MainLayout = lazy(() => import("./Layout/MainLayout/MainLayout"));
const Home = lazy(() => import("./pages/Home/Home"));
const Film = lazy(() => import("./pages/Film/Film"));
const Loading = lazy(() => import("./pages/Loading/Loading"));
const DetailFilm = lazy(() => import("./pages/DetailFilm/DetailFilm"));
const NotFound = lazy(() => import("./pages/NotFound"));

const router = createBrowserRouter([
  {
    element: (
      <Suspense fallback={<Loading />}>
        <MainLayout />
      </Suspense>
    ),
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "phim-sap-chieu",
        element: <Film />,
      },
      {
        path: "phim-dang-chieu",
        element: <Film />,
      },
      {
        path: "dat-ve/:slug",
        element: <DetailFilm />,
      },
      {
        path: "*",
        element: <NotFound />,
      },
      {
        path: "booking/:slug",
        element: <BookingSlug />,
      },
      {
        path: "booking",
        element: <Booking />,
      },
      {
        path: "banking",
        element: <Banking />,
      },
      {
        path: "/payment-form/:bank",
        element: <PaymentForm />,
      },
      {
        path: "/rap-gia-ve/:slug",
        element: <CinemaBySlug />,
      },
      ,
      {
        path: "/tai-khoan/:slug",
        element: <UserDashboard />,
        // children: [
        //   {
        //     path: "profile",
        //     element: <Tabs defaultActiveKey={"2"} items={userItems} />,
        //   },
        //   {
        //     path: "transaction",
        //     element: (
        //       <Tabs
        //         defaultActiveKey={1}
        //         items={userItems}
        //         // onChange={onChange}
        //       />
        //     ),
        //   },
        // ],
      },
    ],
  },
]);

export default function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch({ type: "APP_STARTUP" });
  }, []);

  return <RouterProvider router={router} />;
}
