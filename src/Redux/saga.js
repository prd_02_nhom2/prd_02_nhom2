import { all } from "redux-saga/effects";

import rootCinemaSaga from "./Cinema/cinemaSaga";
import rootFilmSaga from "./Film/filmSaga";
import rootUser from "./User/userSaga";
import rootBookingSaga from "./Booking/bookingDetailSaga";
import rootCitySaga from "./City/citySaga";
import rootTicket from "./Ticket/ticketSaga";

function* rootSaga() {
  yield all([
    rootFilmSaga(),
    rootCinemaSaga(),
    rootUser(),
    rootBookingSaga(),
    rootCitySaga(),
    rootTicket(),
  ]);
}

export default rootSaga;
