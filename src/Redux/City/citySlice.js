import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const citySlice = createSlice({
    name: 'city',
    initialState,
    reducers: {
        fetchCityStart: (state) => {
            state.loading = true;
            state.error = null;
        },
        fetchCitySuccess: (state, action) => {
            state.loading = false;
            state.data = action.payload;
        },
        fetchCityError: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        }
    }
})

export const { fetchCityStart, fetchCitySuccess, fetchCityError } = citySlice.actions
export default citySlice.reducer