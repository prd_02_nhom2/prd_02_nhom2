import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { fetchCityStart, fetchCitySuccess, fetchCityError } from './citySlice';
import { fetchSearchCity } from '../../services/searchApi';


function* fetchCitySaga() {
    try {
        yield delay(500)
        yield put(fetchCityStart())
        const data = yield call(fetchSearchCity);
        yield put(fetchCitySuccess(data));
    } catch (error) {
        yield put(fetchCityError(error));
    }
}

function* rootCitySaga() {
    yield takeEvery('APP_STARTUP', fetchCitySaga);
}

export default rootCitySaga;