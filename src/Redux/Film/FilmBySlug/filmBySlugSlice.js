import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const filmBySlugSlice = createSlice({
    name: 'filmBySlug',
    initialState,
    reducers: {
        fetchFilmBySlugStart: (state) => {
            state.loading = true
            state.error = null
        },
        fetchFilmBySlugSuccess: (state, action) => {
            state.loading = false
            state.data = action.payload
        },
        fetchFilmBySlugFailure: (state, action) => {
            state.loading = false
            state.error = action.payload
        }
    }
})

export const {
    fetchFilmBySlugStart,
    fetchFilmBySlugSuccess,
    fetchFilmBySlugFailure
} = filmBySlugSlice.actions
export default filmBySlugSlice.reducer