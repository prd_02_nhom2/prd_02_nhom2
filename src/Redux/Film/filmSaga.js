import { call, put, takeEvery, delay } from 'redux-saga/effects';
import { fetchFilmSuccess, fetchFilmError, fetchFilmStart } from './filmSlice'
import { fetchMoviesById, fetchFilmBySlug, fetchFilmNowAndSoon } from '../../services/movieApi'
import { fetchMoviesByIdStart, fetchMoviesByIdSuccess, fetchMoviesByIdFailure } from './MoviesById/moviesByIdSlice';
import { fetchFilmBySlugFailure, fetchFilmBySlugStart, fetchFilmBySlugSuccess } from './FilmBySlug/filmBySlugSlice';

function* fetchFilmSaga() {
    try {
        yield delay(500)
        yield put(fetchFilmStart())
        const data = yield call(fetchFilmNowAndSoon);
        yield put(fetchFilmSuccess(data));
    } catch (error) {
        yield put(fetchFilmError(error));
    }
}
function* fetchMoivesByIdSaga(action) {
    try {
        yield put(fetchMoviesByIdStart())
        const response = yield call(fetchMoviesById, action.payload)
        yield put(fetchMoviesByIdSuccess(response.data))
    } catch (error) {
        yield put(fetchMoviesByIdFailure(error))
    }
}
function* fetchFilmBySlugSaga(action) {
    try {
        yield put(fetchFilmBySlugStart())
        const response = yield call(fetchFilmBySlug, action.payload)
        yield put(fetchFilmBySlugSuccess(response.data))
    } catch (error) {
        yield put(fetchFilmBySlugFailure(error))
    }
}
function* rootFilmSaga() {
    yield takeEvery('APP_STARTUP', fetchFilmSaga);
    yield takeEvery('FETCH_MOVIES_BY_ID', fetchMoivesByIdSaga);
    yield takeEvery('FETCH_FILM_BY_SLUG', fetchFilmBySlugSaga);

}

export default rootFilmSaga;