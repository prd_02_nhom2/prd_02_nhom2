import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const moviesByIdSlice = createSlice({
    name: 'moviesById',
    initialState,
    reducers: {
        fetchMoviesByIdStart: (state) => {
            state.loading = true
            state.error = null
        },
        fetchMoviesByIdSuccess: (state, action) => {
            state.loading = false
            state.data = action.payload
        },
        fetchMoviesByIdFailure: (state, action) => {
            state.loading = false
            state.error = action.payload
        }
    }
})

export const {
    fetchMoviesByIdStart,
    fetchMoviesByIdSuccess,
    fetchMoviesByIdFailure
} = moviesByIdSlice.actions
export default moviesByIdSlice.reducer