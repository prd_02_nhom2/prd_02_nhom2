import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  user: {},
  loading: false,
  error: null,
  changePassSuccess: null,
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    // *--=============== Handle Login ===============--
    loginStart: (state, action) => {
      state.loading = true;
      state.error = null;
    },
    loginSuccess: (state, action) => {
      state.loading = false;
      state.user = action.payload;
      // sessionStorage.setItem("user", JSON.stringify(state.user));
    },
    loginFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    setUserFromSession: (state, action) => {
      state.user = action.payload;
    },
    setChangePassSuccess: (state, action) => {
      state.changePassSuccess = action.payload;
    },

    // *--=============== Handle Logout ===============--
    logout: (state) => {
      state.user = {};
      state.loading = false;
      localStorage.clear();
      // sessionStorage.clear();
    },
  },
});

export const {
  loginStart,
  loginSuccess,
  loginFailure,
  setUserFromSession,
  setChangePassSuccess,
  // logout
  logout,
} = userSlice.actions;
export default userSlice.reducer;
