import { call, delay, put, takeEvery } from "redux-saga/effects";
import {
  loginStart,
  loginSuccess,
  loginFailure,
  setChangePassSuccess,
} from "./userSlice";
import { changePassword, login, register } from "../../services/userAPI";
import customAlert from "../../utils/customAlert";

function* HandleLogin(action) {
  try {
    yield put(loginStart());
    const response = yield call(login, action.payload);
    // localStorage.setItem('user', response.data.Email)
    yield delay(500);
    customAlert("Đăng nhập thành công !", "success");
    localStorage.setItem("userInfo", JSON.stringify(response.data));
    yield put(loginSuccess(response.data));
  } catch (error) {
    if (error.message === "Request failed with status code 404") {
      customAlert("Không tìm thấy tài khoản! Vui lòng thử lại!", "error");
    }
    yield put(loginFailure(error));
  }
}
function* HandleChangePassword(action) {
  try {
    yield put(loginStart());
    const response = yield call(changePassword, action.payload);
    yield delay(500);
    customAlert("Thay đổi mật khẩu thành công !", "success");
    yield put(setChangePassSuccess(response.data));
  } catch (error) {
    customAlert("Sai thông tin! Vui lòng thử lại!", "error");
    yield put(loginFailure(error));
  }
}

function* HandleRegister(action) {
  try {
    const response = yield call(register, { ...action.payload, Role: 0 });
    alert("Register Success");
    console.log("HandleRegister : ", response);
  } catch (error) {
    console.log(error);
  }
}

function* rootUser() {
  yield takeEvery("LOGIN", HandleLogin);
  yield takeEvery("REGISTER", HandleRegister);
  yield takeEvery("CHANGEPASS", HandleChangePassword);
}

export default rootUser;
