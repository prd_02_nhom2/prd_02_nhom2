import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  dataSession: [], // lưu dữ liệu (phim/suất/rạp) user đã chọn
  dataMovie: [], // Này là set dữ liệu để show lên cái ticket
  dataSeat: [], // Này là set dữ liệu để show lên ghế đã chọn
  dataCombo: [], // Bắp Nước
  btnTabs: null,
  salary: [],
  finalTicket: {},
  // *--=============== Bất đồng bộ ===============--
  ticketSuccess: {},
  loading: false,
  error: null,
};

let initDataCombo = {
  id: "2acfade1-a4fd-439a-93fe-9236a2d888ce",
  name: "1 bap nuoc",
  price: "129000",
  quantity: 3,
};

let initialFinalTicket = {
  Price: 0,
  ShowCode: "cinemaId-sessionId",
  Email: "user@example.com",
  CinemaName: "string",
  TheaterName: "string",
  FilmName: "The Marvels",
  ImageLandscape: "string",
  ImagePortrait: "string",
  Combo: "string",
  SeatCode: "string",
  ShowTime: "2023-01-13T20:30Z",
};

const ticketBookSlice = createSlice({
  name: "tiketbook",
  initialState,
  reducers: {
    setTicketBookMovie: (state, action) => {
      state.dataMovie = action.payload;
      sessionStorage.setItem('dataMovie', JSON.stringify(action.payload))
    },
    setTicketBookSesstion: (state, action) => {
      state.dataSession = action.payload;
      sessionStorage.setItem('dataSession', JSON.stringify(action.payload))
      sessionStorage.setItem('session', JSON.stringify(state.dataSession.session))

    },
    setTicketBookSeat: (state, action) => {
      if (state.dataSeat.includes(action.payload)) {
        state.dataSeat = state.dataSeat.filter((n) => n != action.payload);
      } else {
        state.dataSeat = [...state.dataSeat, action.payload];
      }
      sessionStorage.setItem('dataSeat', JSON.stringify(state.dataSeat))

    },
    // setTicketBookFoodCombo: (state, action) => {
    //   let temp = state.dataCombo.filter((e) => e.id === action.payload.id)[0];
    //   if (temp) {
    //     state.dataCombo = state.dataCombo.map((e) =>
    //       e.id === action.payload.id
    //         ? { ...e, quantity: action.payload.quantity }
    //         : e
    //     );
    //   } else {
    //     state.dataCombo.push(action.payload);
    //   }
    // },
    addTicketCombo: (state, action) => {
      const combo = state.dataCombo.find(
        (combo) => combo.id === action.payload.id
      );
      if (combo) {
        combo.quantity = action.payload.quantity;
      } else {
        state.dataCombo.push(action.payload);
      }
      sessionStorage.setItem('dataCombo', JSON.stringify(state.dataCombo))
    },
    minusTicketCombo: (state, action) => {
      if (action.payload.quantity === 0) {
        state.dataCombo = state.dataCombo.filter(
          (combo) => combo.id !== action.payload.id
        );
      } else {
        const combo = state.dataCombo.find(
          (combo) => combo.id === action.payload.id
        );
        combo.quantity = action.payload.quantity;
      }
      sessionStorage.setItem('dataCombo', JSON.stringify(state.dataCombo))
    },

    SetFinalTicket: (state, action) => {
      state.finalTicket = action.payload;
      sessionStorage.setItem('finalTicket', JSON.stringify(state.finalTicket))

    },

    setTabs: (state, action) => {
      state.btnTabs = action.payload;
      sessionStorage.setItem('btnTabs', action.payload)
    },
    SetSalary: (state, action) => {
      state.salary = action.payload;
      sessionStorage.setItem('salary', JSON.stringify(state.salary))

    },
    SetChangeSession: (state, action) => {
      state.dataSession.session = action.payload;
      sessionStorage.setItem('session', JSON.stringify(state.dataSession.session))

    },

    // *--=============== Xử lý bất đồng bộ ở đây nhe quý dị===============--
    ticketStart: (state, action) => {
      state.loading = true;
      state.error = null;
    },
    ticketSuccess: (state, action) => {
      state.loading = false;
      state.ticketSuccess = action.payload;
    },
    ticketFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
  },
});

export const {
  setTicketBookMovie,
  setTicketBookSesstion,
  setTicketBookSeat,
  SetFinalTicket,
  setTabs,
  SetSalary,
  SetChangeSession,
  addTicketCombo,
  minusTicketCombo,
  // *--=============== Bất đồng bộ ===============--
  ticketStart,
  ticketSuccess,
  ticketFailure,
} = ticketBookSlice.actions;
export default ticketBookSlice.reducer;
