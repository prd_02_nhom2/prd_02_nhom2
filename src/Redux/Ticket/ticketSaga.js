import { call, delay, put, takeEvery } from "redux-saga/effects";
import { ticketFailure, ticketStart, ticketSuccess } from "./ticketBookSlice";
import {
  fetchTicketByEmail,
  fetchTicketByShowCode,
  postTicket,
} from "../../services/ticketApi";
import customAlert from "../../utils/customAlert";
import {
  fetchTicketShowCodeFailure,
  fetchTicketShowCodeStart,
  fetchTicketShowCodeSuccess,
} from "./TicketByShowCode/ticketByShowCodeSlice";
import {
  fetchTicketEmailStart,
  fetchTicketEmailSuccess,
  fetchTicketEmailFailure,
} from "./TicketByEmail/ticketByEmailSlice";

function* CreateNewTicket(action) {
  try {
    yield put(ticketStart());
    console.log(action.payload);

    const response = yield call(postTicket, action.payload);
    yield delay(300);
    yield console.log(response);
    customAlert("Bạn Đã Mua Vé Thành Công", "success");
    yield put(ticketSuccess(response.data));
  } catch (error) {
    console.log(error.message);
    customAlert("Vui Lòng Thử Lại", "error");
    yield put(ticketFailure(error));
  }
}
//Lay ticket thong qua showcode
function* fetchTicketShowCode(action) {
  console.log(action.payload);

  try {
    yield put(fetchTicketShowCodeStart());
    const data = yield call(fetchTicketByShowCode, action.payload);
    yield put(fetchTicketShowCodeSuccess(data));
  } catch (error) {
    yield put(fetchTicketEmailFailure(error));
  }
}
//lay ticket thong qua email
function* fetchTicketEmail(action) {
  console.log(action.payload);

  try {
    yield put(fetchTicketEmailStart());
    const data = yield call(fetchTicketByEmail, action.payload);
    yield put(fetchTicketEmailSuccess(data));
  } catch (error) {
    yield put(fetchTicketShowCodeFailure(error));
  }
}
function* rootTicket() {
  yield takeEvery("CREATE_NEW_TICKET", CreateNewTicket);
  yield takeEvery("FETCH_TICKET_SHOWCODE", fetchTicketShowCode);
  yield takeEvery("FETCH_TICKET_EMAIL", fetchTicketEmail);
}

export default rootTicket;
