import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const ticketByShowCodeSlice = createSlice({
    name: 'ticketByShowCode',
    initialState,
    reducers: {
        fetchTicketShowCodeStart: (state) => {
            state.loading = true
            state.error = null
        },
        fetchTicketShowCodeSuccess: (state, action) => {
            state.loading = false
            state.data = action.payload
        },
        fetchTicketShowCodeFailure: (state, action) => {
            state.loading = false
            state.error = action.payload
        }
    }
})

export const { fetchTicketShowCodeStart, fetchTicketShowCodeSuccess, fetchTicketShowCodeFailure } = ticketByShowCodeSlice.actions
export default ticketByShowCodeSlice.reducer