import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const ticketByEmailSlice = createSlice({
    name: 'ticketByEmail',
    initialState,
    reducers: {
        fetchTicketEmailStart: (state) => {
            state.loading = true
            state.error = null
        },
        fetchTicketEmailSuccess: (state, action) => {
            state.loading = false
            state.data = action.payload
        },
        fetchTicketEmailFailure: (state, action) => {
            state.loading = false
            state.error = action.payload
        }
    }
})

export const { fetchTicketEmailStart, fetchTicketEmailSuccess, fetchTicketEmailFailure } = ticketByEmailSlice.actions
export default ticketByEmailSlice.reducer