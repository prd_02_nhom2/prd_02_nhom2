import { combineReducers } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { configureStore } from '@reduxjs/toolkit';

import filmReducer from './Film/filmSlice';
import cinemaReducer from './Cinema/cinemaSlice'
import moviesByIdReducer from './Film/MoviesById/moviesByIdSlice';
import filmByCinemaReducer from './Cinema/FilmByCinema/filmByCinemaSlice';
import userReducer from './User/userSlice';
import filmBySlugReducer from './Film/FilmBySlug/filmBySlugSlice';
import rootSaga from './saga';
import bookingDetailReducer from './Booking/bookingDetailSlice';
import ticketBookSlice from './Ticket/ticketBookSlice';
import citySlice from './City/citySlice';
import ticketByShowCodeSlice from './Ticket/TicketByShowCode/ticketByShowCodeSlice';
import ticketByEmailSlice from './Ticket/TicketByEmail/ticketByEmailSlice';

const rootReducer = combineReducers({
    film: filmReducer, //  movie now and soon
    cinema: cinemaReducer, //  all cinema
    moviesById: moviesByIdReducer,
    filmByCinema: filmByCinemaReducer,
    filmBySlug: filmBySlugReducer,
    user: userReducer,
    bookingDetail: bookingDetailReducer,
    ticketbook: ticketBookSlice,
    city: citySlice,
    ticketByShowCode: ticketByShowCodeSlice,
    ticketByEmail: ticketByEmailSlice

});

const sagaMiddleware = createSagaMiddleware()

const store = configureStore({
    reducer: rootReducer,
    middleware: [sagaMiddleware],
})

sagaMiddleware.run(rootSaga)

export default store