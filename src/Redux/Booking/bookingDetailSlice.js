import { createSlice } from "@reduxjs/toolkit"

const initialState = {
    data: [],
    loading: false,
    error: null
}

const bookingDetailSlice = createSlice({
    name: 'bookingDetail',
    initialState,
    reducers: {
        fetchBookingStart: (state) => {
            state.loading = true
            state.error = null
        },
        fetchBookingSuccess: (state, action) => {
            state.loading = false
            state.data = action.payload
        },
        fetchBookingFailure: (state, action) => {
            state.loading = false
            state.error = action.payload
        }
    }
})

export const { fetchBookingStart, fetchBookingSuccess, fetchBookingFailure } = bookingDetailSlice.actions
export default bookingDetailSlice.reducer