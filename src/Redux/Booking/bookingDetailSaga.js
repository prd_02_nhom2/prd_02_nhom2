import { call, put, takeEvery } from "redux-saga/effects";
import { fetchBookingStart, fetchBookingSuccess, fetchBookingFailure } from "./bookingDetailSlice";
import { fetchBookDetail } from "../../services/bookingApi";

function* fetchBookingDetail() {
    try {
        yield put(fetchBookingStart())
        const data = yield call(fetchBookDetail)
        yield put(fetchBookingSuccess(data))
    } catch (error) {
        yield put(fetchBookingFailure(error))
    }
}


function* rootBookingSaga() {
    yield takeEvery('FETCH_BOOKING_DETAIL', fetchBookingDetail)
}

export default rootBookingSaga