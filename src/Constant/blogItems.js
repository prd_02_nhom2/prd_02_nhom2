[
    {
        "id": "25e5e0cd-02ff-409d-80b6-1ee94d8a7b6b",
        "slug": "galaxy-go-mall-ba-ria",
        "name": "Galaxy GO! Mall Bà Rịa",
        "dates": [
            {
                "showDate": "28/10/2023",
                "dayOfWeekLabel": "Thứ bảy",
                "bundles": [
                    {
                        "caption": "sub",
                        "code": "HO00002696",
                        "version": "2d",
                        "sessions": [
                            {
                                "id": "1023-884",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "884",

                                "showDate": "28/10/2023",
                                "showTime": "15:30"
                            },
                            {
                                "id": "1023-878",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "878",

                                "showDate": "28/10/2023",
                                "showTime": "16:00"
                            },

                        ]
                    }
                ]
            },
            {
                "showDate": "29/10/2023",
                "dayOfWeekLabel": "Chủ nhật",
                "bundles": [
                    {
                        "caption": "sub",
                        "code": "HO00002696",
                        "version": "2d",
                        "sessions": [
                            {
                                "id": "1023-927",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "927",

                                "showDate": "29/10/2023",
                                "showTime": "09:30"
                            },
                            {
                                "id": "1023-908",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "908",

                                "showDate": "29/10/2023",
                                "showTime": "10:00"
                            },
                        ]
                    }
                ]
            }
        ]
    },
    {
        "id": "25e5e0cd-02ff-409d-80b6-1ee94d8a7b6b",
        "slug": "galaxy-go-mall-ba-ria",
        "name": "Galaxy GO! Mall Bà Rịa",
        "dates": [
            {
                "showDate": "28/10/2023",
                "dayOfWeekLabel": "Thứ bảy",
                "bundles": [
                    {
                        "caption": "sub",
                        "code": "HO00002696",
                        "version": "2d",
                        "sessions": [
                            {
                                "id": "1023-884",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "884",

                                "showDate": "28/10/2023",
                                "showTime": "15:30"
                            },
                            {
                                "id": "1023-878",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "878",

                                "showDate": "28/10/2023",
                                "showTime": "16:00"
                            },

                        ]
                    }
                ]
            },
            {
                "showDate": "29/10/2023",
                "dayOfWeekLabel": "Chủ nhật",
                "bundles": [
                    {
                        "caption": "sub",
                        "code": "HO00002696",
                        "version": "2d",
                        "sessions": [
                            {
                                "id": "1023-927",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "927",

                                "showDate": "29/10/2023",
                                "showTime": "09:30"
                            },
                            {
                                "id": "1023-908",
                                "cinemaId": "1023",
                                "scheduledFilmId": "HO00002696",
                                "sessionId": "908",

                                "showDate": "29/10/2023",
                                "showTime": "10:00"
                            },
                        ]
                    }
                ]
            }
        ]
    },
]