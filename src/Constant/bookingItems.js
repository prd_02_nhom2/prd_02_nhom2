import Banking from "../components/Banking/Banking";
import BookingByCity from "../components/BookingByCity/BookingByCity";
import FoodCombo from "../components/FoodCombo/FoodCombo";
import Ticket from "../components/Ticket/Ticket";
import BookingDetail from "../pages/Booking/BookingDetail";

export const tabItems = [
  {
    key: 1,
    label: "Chọn phim / Rạp / Suất",
    children: (
      <div className="tabs-item">
        <div className="tabs-item-content">
          <BookingByCity />
        </div>
      </div>
    ),
  },
  {
    key: 2,
    label: "Chọn ghế",
    children: (
      <div className="tabs-item">
        {/* <h1> */}
        <div className="tabs-item-content ">
          <BookingDetail />
        </div>
        {/* </h1> */}
      </div>
    ),
  },
  {
    key: 3,
    label: "Chọn thức ăn",
    children: (
      <div className="tabs-item">
        <div className="tabs-item-content">
          <FoodCombo />
        </div>
      </div>
    ),
  },
  {
    key: 4,
    label: "Thanh toán",
    children: (
      <div className="tabs-item justify-center">
        <div className="tabs-item-content">
          <Banking />
        </div>
      </div>
    ),
  },
  {
    key: 5,
    label: "Xác nhận",
    children: (
      <div className="tabs-item justify-center">
        <div className="tabs-item-content">
          <Ticket hiddenButton />
        </div>
      </div>
    ),
  },
];
