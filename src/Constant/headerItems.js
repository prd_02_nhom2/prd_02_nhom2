import { Link } from "react-router-dom";
// import buyTicket from "../../assets/btn-ticket.jpg";
import buyTicket from "../assets/btn-ticket.jpg";
import { FaClipboardUser } from "react-icons/fa6";
import { FaListOl } from "react-icons/fa";
import { TbLogout2 } from "react-icons/tb";
import { logout } from "../Redux/User/userSlice";


const menuItems = [
    {
        label: (
            <Link className="header-menu-label" to='/booking'>
                <img
                    className="buy-ticket-logo"
                    src={buyTicket}
                    alt="buy-ticket-logo"
                    width="115px"
                />
            </Link>
        ),
        key: 'booking',
        disabled: true
    },
    {
        label: (
            // <Link className="header-menu-label" to={"/phim-dang-chieu"}>
            //     Phim
            // </Link>
            'Phim'
        ),
        path: "/all-film",
        key: "movie",
        children: [
            {
                type: "text",
                label: "Phim Đang chiếu",
                path: "/phim-dang-chieu",
                key: "movie-1",
            }, {
                type: "text",
                label: "Phim Sắp chiếu",
                path: "/phim-sap-chieu",
                key: "movie-2",
            },

        ],
    },
    {
        label: (
            // <Link className="header-menu-label" to={"/"}>
            //     Blog
            // </Link>
            "Blog"
        ),
        key: "blog",
        children: [
            {
                type: "text",
                label: "Thể loại phim",
                key: "blog-1",
            },
            {
                type: "text",
                label: "Diễn viên",
                key: "blog-2",
            },
            {
                type: "text",
                label: "Đạo diễn",
                key: "blog-3",
            },
            {
                type: "text",
                label: "Bình luận viên",
                key: "blog-4",
            },
            {
                type: "text",
                label: "Blog điện ảnh",
                key: "blog-5",
            },
        ],
    },
    {
        label: (
            <Link className="header-menu-label" to={"/"}>
                Sự kiện
            </Link>
        ),
        key: "promotion",
        children: [
            {
                type: "text",
                label: "Ưu đãi",
                key: "promotion-1",
            },
            {
                type: "text",
                label: "Phim hay tháng",
                key: "promotion-2",
            },
        ],
    },
    {
        label: (
            <Link className="header-menu-label" to={'/'}>
                Rạp /Giá Vé
            </Link >
        ),
        key: "rap-gia-ve",
        children: [
            {
                type: "text",
                label: "Galaxy Nguyen Du",
                key: "galaxy-nguyen-du",
            },
            {
                type: "text",
                label: "Phim hay tháng",
                key: "ram-name-2",
            },
        ],
    },
];

export const headerItems = {
    menuItems: menuItems,
}