import History from "../components/UserDashboard/History";
import InforForm from "../components/UserDashboard/InforForm";

export const userItems = [
  {
    key: "1",
    label: "Lịch Sử Giao dịch",
    children: <History />,
  },
  {
    key: "2",
    label: "Thông Tin Cá Nhân",
    children: <InforForm />,
  },
  {
    key: "3",
    label: "Thông Báo",
    children: "Comming Soon",
  },
  {
    key: "4",
    label: "Quà Tặng",
    children: "Comming Soon...",
  },
  {
    key: "5",
    label: "Chính Sách",
    children: "Comming Soon...",
  },
];
