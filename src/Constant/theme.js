export const theme = {
    // Global Token
    // *--=============== GLOBAL TOKEN ===============--
    token: {
        // Seed Token
        colorPrimary: "#f58020",
        colorInfo: "#f58020",
        borderRadius: 6,
        colorLink: '#1677ff',
        colorLinkActive: "#F58020",
        colorLinkHover: "#f58020",
        colorBgTextHover: '#fb770b1a',

    },
    // *--=============== CSS COMPONENT TOKEN ===============--

    components: {
        Button: {
            // colorPrimary: '#f58020',
            // defaultBorderColor: '#F58020',
            // defaultColor: 'white',
            // colorBorder: "red",
            // textHoverBg: '#fb770b1a',
            algorithm: true, // Enable algorithm
        },
        Tabs: {
            /* here is your component tokens */
            horizontalItemMargin: '200',
            horizontalItemPadding: '19px 0',
            horizontalItemGutter: '23',
            itemSelectedColor: '#034ea2',
            inkBarColor: "#034ea2",
            itemHoverColor: "none",
            itemActiveColor: "none",
            titleFontSize: 18,
            // algorithm: true
        },
        Menu: {
            /* here is your component tokens */
            itemHoverBg: '#fb770b1a',
            itemHoverColor: '#f26b38',
        },
        Input: {
            algorithm: true
        }
    }


}



export const buyTicketQuicklyConfig = {
    components: {
        Button: {
            "colorBorder": "#ffbb73",
            borderRadius: 0,
            "colorBgContainerDisabled": "#ffbb73",
            "algorithm": true,
        },
        Select: {
            "algorithm": true,
            "colorBgContainerDisabled": "rgba(255, 255, 255, 0.04)",
            "colorBorder": "rgb(255, 255, 255)",
            borderRadius: 0,
            colorBgContainerDisabled: "white",
        }
    }
}