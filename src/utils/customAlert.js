import { message } from "antd";
const customAlert = (content, type = "info") => {
  // message.success('This is a success message');
  // message.error('This is a error message');
  // message.info('This is a info message');
  // message.warning('This is a warning message');
  message[type](content);
};

export default customAlert;

// Save Temp Variable
let initDataChoose = {
  slug: "dat-rung-phuong-nam",
  cine: "cine", // vị trí rạp
  showDate: "showDate", // "07/11/2023"
  session: "session", // "19:00"

  // thông tin này bị thiếu
  caption: "caption", // phim vietsub hay long tieng
  vertion: "vertion", // loại vé 2d hay ...
  screenName: "screenName", // "RAP 4"

  // ------------ TicketBookMovie ------------
  age: "18",
  img: "https://cdn.galaxycine.vn/media/2023/11/3/500x750-nvcc_1698985267862.jpg",
  name: "Người Vợ Cuối Cùng",
};

let initTicketBookMovie = {
  age: "18",
  img: "https://cdn.galaxycine.vn/media/2023/11/3/500x750-nvcc_1698985267862.jpg",
  name: "Người Vợ Cuối Cùng",
};
