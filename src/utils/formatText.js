import moment from "moment";

// nhận vào phải là một number / string
export const formatPrice = (number) => {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

// nhận vào phải là một array nếu không sẽ bị lỗi
export const formatChairList = (array) => {
  return array.map((e) => `${e},`);
};

// console.log(formatPrice); // Output: "1.234.567.890"

// Covert Date Save Into Datadase
export const formatDate = (date) => {
  // Định dạng ngày 15/11/2023 13:12
  return moment(date, "DD/MM/YYYY hh:mm").format("YYYY-MM-DDThh:mm[Z]");
  //console.log(moment("15/11/2023 13:12", "DD/MM/YYYY hh:mm").format("YYYY-MM-DDThh:mm[Z]"));
};
