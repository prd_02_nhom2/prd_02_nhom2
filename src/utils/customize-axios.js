import axios from "axios";

const REACT_APP_API_URL =
  "https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/cinema";
const REACT_APP_API_USER_URL =
  "https://teachingserver.org/U2FsdGVkX18MaY1VB6bVfvVBm0wdPflO/user";

const apiUrl = REACT_APP_API_URL;
const apiUserUrl = REACT_APP_API_USER_URL;

export const api = axios.create({
  baseURL: apiUrl,
  headers: { accept: "application/json", "Content-Type": "application/json" },
});

export const apiUser = axios.create({
  baseURL: apiUserUrl,
  headers: { accept: "application/json", "Content-Type": "application/json" },
});
