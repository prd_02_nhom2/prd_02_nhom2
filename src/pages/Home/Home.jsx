import React from 'react'
import Carousel from './Carousel/Carousel'
import TabFilm from './TabFilm/TabFilm'
import { Footer } from 'antd/es/layout/layout'
import New from '../../components/New/New'

export default function Home() {
    sessionStorage.clear()
    return (
        <div>
            <Carousel />
            <TabFilm film={8} />
            <New />
            <Footer />
        </div>
    )
}
