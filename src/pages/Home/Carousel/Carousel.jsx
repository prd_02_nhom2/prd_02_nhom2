import React from 'react'
import './carousel.scss'
import { sliderItems } from '../../../Constant/sliderItems';
import Slider from 'react-slick';
import BuyTicketQuickly from '../TicketQuickly/BuyTicketQuickly';


const settings = {
    // CSS cho >= 1400px
    dots: true,
    centerMode: true,
    slidesToShow: 1,
    speed: 500,
    arrows: true,
    slidesToScroll: 1,
    focusOnSelect: true,
    // variableWidth: true,
    responsive: [
        {
            breakpoint: 1200,
            settings: {
                variableWidth: true,
                centerMode: true,
                slidesToShow: 1,
                dots: true,
                speed: 500,
                slidesToScroll: 1,
            }
        },
        {
            breakpoint: 1024,
            settings: {
                centerMode: false,
            }
        },
    ]
};

const renderSlides = sliderItems.map((item, index) => (
    <div key={index + 1}>
        <img className="carosel-imgage" src={item.imageUrl} alt="" />
    </div>
));
export default function Carousel() {
    return (
        <section id='Carousel'>
            <Slider {...settings}>{renderSlides}</Slider>
            <BuyTicketQuickly />
        </section>
    )
}
