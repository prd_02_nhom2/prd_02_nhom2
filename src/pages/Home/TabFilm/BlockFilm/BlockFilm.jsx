import { memo, useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { FaTicketAlt, FaPlayCircle  } from "react-icons/fa";
import { faStar } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom';
import Modal from 'react-modal';
import '../tabfilm.scss';
import './BlockFilm.scss'
import TrailerFilm from '../../../DetailFilm/TrailerFilm/TrailerFilm';
import { useDispatch } from 'react-redux';
import { setTabs } from '../../../../Redux/Ticket/ticketBookSlice';

function BlockFilm({ film }) {
    const dispatch = useDispatch()
    const nav = useNavigate()
    const [modalIsOpen, setModalIsOpen] = useState(false);
    const openModal = () => {
        setModalIsOpen(true);
    };

    const closeModal = () => {
        setModalIsOpen(false);
    };
    const HandleBuy = () => {
        dispatch(setTabs(2))
        nav(`/dat-ve/${film.slug}`, { replace: true })
    }
    return (
        <div className='showing__film ' key={film.id}>
            <div className="showing__film--movie">
                <img className='showing__film--image' src={film.imagePortrait} alt="image" />
                <div className='showing__film--point '>
                    <div style={{ "display": "flex", "alignItems": "center" }}>
                        <FontAwesomeIcon icon={faStar} style={{ color: "#ddfa00", }} />
                        <p style={{ "fontWeight": 700, "color": "#ffffff" }}>{(film.point).toFixed(1)}</p>
                    </div>
                    <p className="p-T">{('T' + film.age).toUpperCase()}</p>
                </div>
                <div className="showing__film--hover ">
                    <button onClick={() => HandleBuy()} className="btnBuy"><FaTicketAlt/>Mua vé</button>
                    <button onClick={openModal} className="btnTrailer"> <FaPlayCircle />Trailer</button>
                    <Modal isOpen={modalIsOpen}
                        onRequestClose={closeModal}
                        className="modal-content"
                        overlayClassName="modal-overlay">
                        <TrailerFilm slug={film.slug} width={"1280px"} height={"720px"} play={true} />
                    </Modal>
                </div>
            </div>
            <div className="showing__film--title">
                <h3 style={{ "textTransform": "capitalize" }} className='uppercase'>{film.name}</h3>
                <p className='showing__film--paragraph '>{film.subName}</p>
            </div>
        </div>
    )
}
export default memo(BlockFilm)