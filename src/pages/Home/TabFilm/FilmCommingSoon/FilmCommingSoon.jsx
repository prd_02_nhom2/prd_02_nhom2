import { memo } from "react"
import { useSelector } from "react-redux"
import BlockFilm from "../BlockFilm/BlockFilm"
import { Link } from "react-router-dom"
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faArrowRight } from "@fortawesome/free-solid-svg-icons"
import '../tabfilm.scss'

function FilmCommingSoon(props) {
    const { countRender } = props
    const film = useSelector(state => state.film.data.data?.movieCommingSoon)

    return (
        <div className='showing'>
            {
                !countRender ?
                    film?.map(film => (
                        <BlockFilm key={film.id} film={film} />
                    )) :
                    film?.slice(0, countRender).map(film => (
                        <BlockFilm key={film.id} film={film} />
                    ))
            }
        </div>
    )
}
export default memo(FilmCommingSoon)