// FilmCommingSoon.js
import { memo } from "react";
import { useSelector } from "react-redux";
import BlockFilm from "../BlockFilm/BlockFilm";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import '../tabfilm.scss';
import BlockFlimHeader from "../BlockFilm/BlockFlimHeader";

function FilmCommingSoonHeader(props) {
    const film = useSelector(state => state.film.data.data?.movieCommingSoon);

    // Display only the first four movies
    const moviesToDisplay = film?.slice(0, 4);

    return (
        <div className='showing'>
            {moviesToDisplay?.map(film => (
                <BlockFlimHeader key={film.id} film={film} />
            ))}
        </div>
    );
}

export default memo(FilmCommingSoonHeader);
