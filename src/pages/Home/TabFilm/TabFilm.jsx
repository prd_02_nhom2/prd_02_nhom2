import { Tabs } from 'antd';
import { Link, useNavigate } from 'react-router-dom';

import FilmShowing from './FilmShowing/FilmShowing';
import FilmCommingSoon from './FilmCommingSoon/FilmCommingSoon';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

import './tabfilm.scss'

export default function TabFilm(props) {
    let key
    const { film } = props
    const items = [
        {
            key: '1',
            label: 'Phim đang chiếu',
            children: <FilmShowing countRender={film} />,
        },
        {
            key: '2',
            label: 'Phim sắp chiếu',
            children: <FilmCommingSoon countRender={film} />,
        }
    ]
    const nav = useNavigate()
    const HandleMore = (key) => {
        nav(`/phim-dang-chieu`, { replace: true })
    }
    return (
        <section className='tabFilm'>
            <div className='containerTabFilm '>
                <Tabs defaultActiveKey="1" items={items} activeKey={key} />
                {
                    film && <button className='tabFilm__button ' onClick={() => HandleMore()}>
                        Xem Thêm <FontAwesomeIcon icon={faArrowRight} />
                    </button>
                }
            </div>
        </section>
    )
}
