import React, { memo, useEffect, useState } from "react";
import "./BuyTicketQuickly.scss";
import { Button, ConfigProvider, Select, Space } from "antd";
import { SearchOutlined } from "@ant-design/icons";
import {
  PiNumberOneBold,
  PiNumberTwoBold,
  PiNumberThreeBold,
  PiNumberFourBold,
} from "react-icons/pi";
import { useDispatch, useSelector } from "react-redux";
import ModalLogin from "../../../components/ModalLogin/ModalLogin";
import { Group } from "antd/es/avatar";
import { buyTicketQuicklyConfig } from "../../../Constant/theme";
import { useNavigate } from "react-router-dom";
import {
  setTabs,
  setTicketBookMovie,
  setTicketBookSesstion,
} from "../../../Redux/Ticket/ticketBookSlice";

function BuyTicketQuickly() {
  const nav = useNavigate();
  const dispatch = useDispatch();

  const userRedux = useSelector((state) => state.user.user);
  const film = useSelector((state) => state.film.data.data?.movieShowing);
  const moviesById = useSelector((state) => state.moviesById.data);

  const [day, setDay] = useState([]);
  const [bundle, SetBundle] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [submitObject, setSubmitObject] = useState();
  const [hasNavAddress, setHasNavAddress] = useState("");

  {
    /* --=============== 1. LOAD LIST FILM =============== */
  }
  const HandleSelectFilm = (value, option) => {
    dispatch({ type: "FETCH_MOVIES_BY_ID", payload: value });
    setSubmitObject((pre) => ({
      ...pre,
      slug: option.slug,
      age: option.age,
      img: option.img,
      name: option.name,
      imageLandscape: option.imageLandscape,
    }));
  };

  {
    /* --=============== 2. LOAD CINEMA CITY =============== */
  }
  const HandleSelectCinema = (value, option) => {
    setDay(
      moviesById.filter((film) => film.id === value).map((items) => items.dates)
    );
    setSubmitObject((pre) => ({ ...pre, cine: option.cine }));
    // console.log(option)
  };

  {
    /* --=============== 3. "Thứ 4, 07/11/2023" =============== */
  }
  const HandleSelectDay = (value, option) => {
    let daySelect = day[0]
      .filter((n) => n.showDate === value)
      .map((n) => n.bundles);
    SetBundle(daySelect[0][0]);
    setSubmitObject((pre) => ({
      ...pre,
      showDate: option.showDate,
      caption: option.caption,
      vertion: option.vertion,
    }));
  };

  {
    /* --=============== 4. "19:00" =============== */
  }
  const HandleSeletTime = (value, option) => {
    setSubmitObject((pre) => ({
      ...pre,
      session: option.session,
      screenName: option.screenName,
    }));
  };

  {
    /* --=============== 5.SUBMIT BUTTON =============== */
  }
  const HandleSubmitTicket = (value) => {
    // Check Login First
    if (!userRedux.Name) {
      setIsModalOpen(true);
      setHasNavAddress(`/booking/${submitObject.slug}`);
    } else {
      nav(`/booking/${submitObject.slug}`, { replace: true });
    }

    let dataChoose = {
      cine: { name: submitObject.cine },
      bundle: {
        caption: submitObject.caption,
        version: submitObject.vertion,
        sessions: bundle.sessions,
      },
      session: {
        showTime: submitObject.session, // "19:00"
        screenName: submitObject.screenName, // "RAP 4"
        showDate: submitObject.showDate,
      }, // "07/11/2023"
    };
    dispatch(setTicketBookSesstion(dataChoose));
    dispatch(
      setTicketBookMovie({
        name: submitObject.name,
        age: submitObject.age,
        img: submitObject.img,
        imageLandscape: submitObject.imageLandscape,
      })
    );
    dispatch(setTabs(2));
  };

  return (
    <>
      <ConfigProvider theme={buyTicketQuicklyConfig}>
        <div id="BuyTicketQuickly">
          {/* --=============== CHỌN PHIM =============== */}
          <Select
            icon={<SearchOutlined />}
            className="select-ticket"
            placeholder={
              <div className="ticket-quickly-title">
                {" "}
                <PiNumberOneBold className="orange" />
                <span>Chọn Phim</span>
              </div>
            }
            onChange={HandleSelectFilm}
            options={film?.map((items) => ({
              value: items.id,
              label: items.name,
              slug: items.slug,

              age: items.age,
              img: items.imagePortrait,
              name: items.name,
              imageLandscape: items.imageLandscape,
            }))}
          />

          {/* --=============== RẠPPPPPPPP =============== */}
          <Select
            // disabled={!moviesById.length}
            className="select-ticket"
            placeholder={
              <div className="ticket-quickly-title">
                {" "}
                <PiNumberTwoBold className="orange" />
                <span>Chọn Rạp</span>
              </div>
            }
            onChange={HandleSelectCinema}
            options={moviesById?.map((items) => ({
              value: items.id,
              label: items.name,
              cine: items.name,
              // items: items
            }))}
          />

          {/* --=============== CHỌN NGÀY XEM =============== */}
          <Select
            // disabled={!day[0]}
            className="select-ticket"
            placeholder={
              <div className="ticket-quickly-title">
                {" "}
                <PiNumberThreeBold className="orange" />
                <span>Chọn Ngày</span>
              </div>
            }
            onChange={HandleSelectDay}
            options={day[0]?.map((items) => ({
              value: items.showDate,
              label: items.dayOfWeekLabel + ", " + items.showDate,
              caption: items.bundles[0].caption,
              vertion: items.bundles[0].version,
              showDate: items.showDate,
            }))}
          />

          {/* --=============== SUẤT CHIẾU =============== */}
          <Select
            // disabled={!bundle}
            className="select-ticket"
            placeholder={
              <div className="ticket-quickly-title">
                {" "}
                <PiNumberFourBold className="orange" />
                <span>Chọn Suất</span>
              </div>
            }
            onChange={HandleSeletTime}
            style={{
              width: 120,
            }}
            options={
              bundle &&
              bundle.sessions.map((n) => ({
                value: n.showTime,
                label: n.showTime,
                session: n.showTime,
                screenName: n.screenName,
              }))
            }
          />
          <Button
            disabled={!submitObject}
            className="select-ticket buyTicketQuicklyButton"
            onClick={HandleSubmitTicket}
            type="primary"
          >
            Mua vé nhanh lên
          </Button>
        </div>
      </ConfigProvider>
      <ModalLogin
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        hasNavAddress={hasNavAddress}
      />
    </>
  );
}

export default memo(BuyTicketQuickly);
