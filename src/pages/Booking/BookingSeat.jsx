import React, { useEffect, useState } from "react";
import "./BookingSlug.scss";
import { useDispatch, useSelector } from "react-redux";
import {
  setTicketBookSeat,
  SetSalary,
} from "../../Redux/Ticket/ticketBookSlice";

export default function BookingSeat(props) {
  const salary = useSelector(
    (n) => n.bookingDetail?.data.data.ticket[0].displayPrice
  );

  const dispatch = useDispatch();
  const rows = props.rows;
  const ls = props.ls;
  const i = props.i;
  const [j, setJ] = useState(props.j);

  const [selected, setSelected] = useState(undefined);
  const [checked, setChecked] = useState(undefined);
  console.log();


  useEffect(() => {
    if ([3, 5, 7, 9].includes(i)) {
      if (j > 4) {
        j > 21 ? setJ(j - 3) : setJ(j - 1);
      }
    } else if ([2, 4, 6, 8].includes(i)) {
      if (j > 4) {
        j > 22 ? setJ(j - 2) : setJ(j - 1);
      }
    } else if ([11, 12, 13, 14, 15].includes(i)) {
      setJ(j - 1);
    } else if (i === 0) {
      setJ(j - 5);
    } else {
      setJ(j - 2);
    }
    if (props.showCode == '') {

    } else {
      if (eval('(' + props.showCode + ')').includes(" " + rows[i].physicalName + rows[i].seats[j]?.id)) {
        setChecked(true)
      }
    }

  }, []);
  const HandlePosition = (name, value, j) => {
    dispatch(setTicketBookSeat(" " + name + value));
    if (selected === j) {
      setSelected(null);
    } else {
      setSelected(j);
    }
    dispatch(SetSalary(salary));
  };
  return (
    <span
      style={checked && { pointerEvents: 'none', background: 'gray' }}
      key={j}
      className={j === selected ? "selected" : ""}
      onClick={() =>
        HandlePosition(rows[i].physicalName, rows[i].seats[j]?.id, j)
      }
    >
      {rows[i].seats[j]?.id}
    </span>
  );
}
