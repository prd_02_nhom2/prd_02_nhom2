import React, { useCallback, useEffect, useState } from "react";

import "./BookingDetail.scss";
import { useDispatch, useSelector } from "react-redux";
import BookingSeat from "./BookingSeat";
import { SetChangeSession } from "../../Redux/Ticket/ticketBookSlice";

export default function BookingDetail(props) {
  const dispatch = useDispatch();
  const bookingDetailData = useSelector((n) => n.bookingDetail?.data.data);
  const areas = bookingDetailData?.seatPlan.seatLayoutData.areas[0];
  const areas1 = bookingDetailData?.seatPlan.seatLayoutData.areas[1];
  const dataTicket = useSelector(
    (state) => state.ticketbook.dataSession.bundle?.sessions
  );
  const ShowTimeCheck = useSelector(
    (state) => state.ticketbook.dataSession?.session?.showTime
  );
  const ticketByShowCode = useSelector(state => state.ticketByShowCode?.data.data)
  const showCode = ticketByShowCode?.map(n => n.SeatCode)

  const rows1 = bookingDetailData?.seatPlan.seatLayoutData.areas[0].rows;
  const rows2 = bookingDetailData?.seatPlan.seatLayoutData.areas[1].rows;
  const rows = [...rows2, ...rows1];

  const [structure, SetStructure] = useState();
  const [ls, SetLs] = useState();
  const [checked, SetChecked] = useState(ShowTimeCheck);
  useEffect(() => {
    SetLs(
      rows.map((n) =>
        n.seats.map((m) => ({
          row: m.position.rowIndex,
          col: m.position.columnIndex,
        }))
      )
    );
    SetStructure({
      row: areas.rowCount + areas1.rowCount,
      col: areas.columnCount,
    });
  }, [props]);
  const HandleChangeSession = (session) => {
    dispatch(SetChangeSession(session));
    SetChecked(session.showTime);
  };

  return (
    structure && (
      <div className="cinema-position">
        <div className="cinema-sesstion">
          <h4 className="change-movie-time">Đổi suất chiếu: </h4>
          <div className="allBtnShowTime">
            {dataTicket &&
              dataTicket.map((sesstion, i3) => {
                return (
                  <button
                    className={
                      checked === sesstion.showTime
                        ? "btnShowTime checked"
                        : "btnShowTime"
                    }
                    onClick={() => HandleChangeSession(sesstion)}
                    key={i3}
                  >
                    {sesstion.showTime}
                  </button>
                );
              })}
          </div>
        </div>
        <div className="row-position border-dashed">
          {Array(structure.row)
            .fill(0)
            ?.map((n, i) => {
              return (
                <div key={i} className="row-position-content">
                  <span
                    className={
                      !rows[i].physicalName ? "non-border" : "spanName"
                    }
                  >
                    {rows[i].physicalName}
                  </span>
                  <div className="seat-list">
                    {Array(structure.col)
                      .fill(0)
                      .map((m, j) => {
                        return ls[i].find((k) => k.col === j) ? (
                          <BookingSeat
                            rows={rows}
                            ls={ls}
                            i={i}
                            j={j}
                            key={j}
                            showCode={showCode}
                          />
                        ) : (
                          <span key={j} className={"non-border"}>
                            {""}
                          </span>
                        );
                      })}
                  </div>
                  <span
                    className={
                      !rows[i].physicalName ? "non-border" : "spanName"
                    }
                  >
                    {rows[i].physicalName}
                  </span>
                </div>
              );
            })}
        </div>
      </div>
    )
  );
}
