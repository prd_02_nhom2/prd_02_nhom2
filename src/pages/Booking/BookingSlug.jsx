import React, { useCallback, useEffect, useState } from "react";

import "./BookingSlug.scss";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Ticket from "../../components/Ticket/Ticket";
import { tabItems } from "../../Constant/bookingItems";
import Divider from "../../components/Divider/Divider";
import { ConfigProvider, Tabs } from "antd";

export default function BookingSlug() {
  const dispatch = useDispatch();
  const { slug } = useParams();
  const bookingDetailData = useSelector((n) => n.bookingDetail?.data.data);
  const dataTicket = useSelector((state) => state.ticketbook);
  const btnTabs = dataTicket?.btnTabs;

  useEffect(() => {
    dispatch({
      type: "FETCH_BOOKING_DETAIL",
    });
    dispatch({
      type: "FETCH_TICKET_SHOWCODE",
      payload: dataTicket.dataSession.session?.cinemaId + '-' + dataTicket.dataSession.session?.id
    });
  }, []);
  const onChange = (key) => { };
  return (
    bookingDetailData && (
      <>
        <Divider />
        <div className="BookingSlug">
          <div className="bookingSlugTabs ">
            <Tabs
              className="BookingDetail"
              centered
              activeKey={parseInt(sessionStorage.getItem('btnTabs'))}
              items={tabItems}
              onChange={onChange}
              tabBarExtraContent={
                ([4, 5].includes(parseInt(sessionStorage.getItem('btnTabs')))) || (
                  <div className="tabs-extra-booking-ticket">
                    <Ticket keyName={parseInt(sessionStorage.getItem('btnTabs'))} />
                  </div>
                )
              }
            />
          </div>
        </div>
      </>
    )
  );
}
