import React, { useEffect } from "react";
import "./Booking.scss";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Ticket from "../../components/Ticket/Ticket";
import { tabItems } from "../../Constant/bookingItems";
import Divider from "../../components/Divider/Divider";
import { ConfigProvider, Tabs } from "antd";

export default function Booking() {
  const dataTicket = useSelector((state) => state.ticketbook);
  const btnTabs = dataTicket?.btnTabs;
  const onChange = () => { };
  return (
    <>
      <Divider />
      <div id="Booking">
        <div className="bookingSlugTabs">
          <Tabs
            className="BookingDetail"
            centered
            activeKey={parseInt(sessionStorage.getItem('btnTabs'))}
            items={tabItems}
            onChange={onChange}
            tabBarExtraContent={
              <div className="tabs-extra-booking-ticket">
                <Ticket keyName={parseInt(sessionStorage.getItem('btnTabs'))} />
              </div>
            }
          />
        </div>
      </div>
    </>
  );
}
