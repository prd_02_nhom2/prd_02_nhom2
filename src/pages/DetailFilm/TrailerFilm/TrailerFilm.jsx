import React from 'react'
import ReactPlayer from 'react-player';
import { useSelector } from 'react-redux';

export default function TrailerFilm(props) {
    const filmShowingData = useSelector(state => state.film.data.data?.movieShowing)
    const filmComingSoonData = useSelector(state => state.film.data.data?.movieCommingSoon)
    const filmFilter1 = filmShowingData?.filter(n => n.slug === props.slug)[0]?.trailer
    const filmFilter2 = filmComingSoonData?.filter(n => n.slug === props.slug)[0]?.trailer
    return (
        <div>
            {filmFilter1 && <ReactPlayer
                url={
                    filmFilter1
                }
                width={props.width ? props.width : '960px'}
                height={props.height ? props.height : '540px'}
                playing={props.play ? props.play : false}
                controls={true}
            />}
            {filmFilter2 && <ReactPlayer
                url={
                    filmFilter2
                }
                width={props.width ? props.width : '960px'}
                height={props.height ? props.height : '540px'}
                playing={props.play ? props.play : false}
                controls={true}
            />}
        </div>
    )
}
