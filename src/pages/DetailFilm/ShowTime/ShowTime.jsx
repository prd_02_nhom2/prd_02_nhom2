import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./ShowTime.scss";
import { useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import {
  setTicketBook,
  setTicketBookMovie,
  setTicketBookSesstion,
} from "../../../Redux/Ticket/ticketBookSlice";
import ModalLogin from "../../../components/ModalLogin/ModalLogin";

export default function ShowTime(props) {
  const { slug } = useParams();
  const nav = useNavigate();
  const dispatch = useDispatch();
  const [datesData, SetDatesData] = useState([]);
  const [cityData, SetCityData] = useState([]);
  const [cineData, SetCineData] = useState([]);
  const [showTimePerDay, setShowTimePerDay] = useState();
  const [softTimePerDay, setSoftTimePerDay] = useState([]);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [hasNavAddress, setHasNavAddress] = useState("");

  const [selectedDay, setSelectedDay] = useState(null);

  const userRedux = useSelector((state) => state.user.user);
  const film = useSelector((film) => film.filmBySlug.data);
  const allCityData = useSelector((city) => city.city.data.data)?.map(
    (city) => ({ name: city.name, id: city.id })
  );
  const cityFilter = allCityData?.filter((n) => cityData.includes(n.id));

  // const [daysData, SetDaysData] = useState([])
  // const [bundles, SetBundles] = useState(['12h22', '13h33'])
  useEffect(() => {
    SetDatesData(movieByIdData.map((n) => n.dates));
    SetCityData(movieByIdData.map((n) => n.cityId));
    SetCineData(movieByIdData.map((n) => n.name));
  }, [props]);

  const movieByIdData = props.data;
  const datesAllCinema = movieByIdData?.map((n) => n.dates);
  const sessionsDatefilter = datesAllCinema?.find(
    (arr) =>
      arr.length === Math.max(...datesAllCinema?.map((arr) => arr.length))
  );
  const handleDay = (showDate, index) => {
    setSelectedDay(index);
    setShowTimePerDay(
      movieByIdData.filter((n) =>
        n.dates.map((m) => m.showDate).includes(showDate)
      )
    );
    setSoftTimePerDay(
      movieByIdData.map((n) =>
        n.dates.filter((date) => date.showDate === showDate)
      )
    );
    // SetBundles(movieByIdData.filter(n => n.dates[0].showDate === showDate))
  };
  const handleCity = (e) => {
    let dataMovies = movieByIdData.filter((n) => n.cityId === e.target.value);
    setShowTimePerDay(dataMovies);
    SetCineData(dataMovies.map((cine) => cine.name));
  };
  const handleCine = (e) => {
    let dataCine = movieByIdData.filter((n) => n.name === e.target.value);
    setShowTimePerDay(dataCine);
  };

  const [checked, setChecked] = useState("");
  const handleClickSesstion = (cine, session, bundle, sesstion) => {
    console.log(sesstion.id);
    setChecked(sesstion.id);
    if (!userRedux.Name) {
      setIsModalOpen(true);
      props.idDatVePath &&
        setHasNavAddress(`/booking/${slug}`, { replace: true });
    } else {
      props.idDatVePath && nav(`/booking/${slug}`, { replace: true });
    }

    let dataChoose = {
      cine: cine,
      session: session,
      bundle: bundle,
    };
    dispatch(setTicketBookSesstion(dataChoose));
    props.idDatVePath &&
      dispatch(
        setTicketBookMovie({
          name: film.name,
          age: film.age,
          img: film.imagePortrait,
          imageLandscape: film.imageLandscape,
        })
      );
  };
  return (
    sessionsDatefilter && (
      <div className="ShowTime">
        <div className="dayList">
          <ul className="days">
            {sessionsDatefilter.map((n, i) => {
              return (
                <li
                  key={i}
                  className={i === selectedDay ? "selected day" : "day"}
                  onClick={() => handleDay(n.showDate, i)}
                >
                  <p>{n.dayOfWeekLabel}</p>
                  {n.showDate}
                </li>
              );
            })}
          </ul>
          <select onChange={(e) => handleCity(e)}>
            <option>Toàn Quốc</option>
            {cityFilter?.map((city, i) => {
              return (
                <option key={i} value={city.id}>
                  {city.name}
                </option>
              );
            })}
          </select>
          <select onChange={(e) => handleCine(e)}>
            <option>Tất cả rạp</option>
            {cineData.map((cine, i) => {
              return (
                <option key={i} value={cine}>
                  {cine}
                </option>
              );
            })}
          </select>
        </div>
        <div className="showtimeListBox">
          {showTimePerDay
            ? showTimePerDay.map((cine, i1) => {
                return (
                  softTimePerDay[i1][0] && (
                    <div
                      key={i1}
                      className="showTimebox"
                      style={{ padding: "10px 0px" }}
                    >
                      <h4>{cine.name}</h4>
                      {softTimePerDay[i1][0].bundles.map((bundle, i2) => {
                        return (
                          <div className="sesstionBox" key={i2}>
                            <div className="caption">
                              <p>{bundle.version}</p>
                              <p>
                                {bundle.caption === "sub"
                                  ? "Phụ đề"
                                  : "Lồng tiếng"}
                              </p>
                            </div>
                            <div>
                              {bundle.sessions.map((sesstion, i3) => {
                                return (
                                  <button
                                    className={`btnShowTime ${
                                      checked === sesstion.id && "is-Checked"
                                    }`}
                                    key={i3}
                                    onClick={() =>
                                      handleClickSesstion(
                                        cine,
                                        sesstion,
                                        bundle,
                                        sesstion
                                      )
                                    }
                                  >
                                    {sesstion.showTime}
                                  </button>
                                );
                              })}
                            </div>
                          </div>
                        );
                      })}
                    </div>
                  )
                );
              })
            : ""}
          <ModalLogin
            isModalOpen={isModalOpen}
            setIsModalOpen={setIsModalOpen}
            hasNavAddress={hasNavAddress}
          />
        </div>
      </div>
    )
  );
}
