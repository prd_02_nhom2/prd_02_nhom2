import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate, useParams } from 'react-router-dom';
import './CinemaBySlug.scss'
import ShowingByCinema from '../../components/ShowingBycinema/ShowingByCinema';
import Carousel from '../Home/Carousel/Carousel';

export default function CinemaBySlug() {
    const dispatch = useDispatch()
    const { slug } = useParams()
    const nav = useNavigate()

    const [cityData, SetCityData] = useState([])
    const [cineData, SetCineData] = useState([])


    const cinemaAllData = useSelector(cine => cine.cinema?.data.data)
    const cinemaFilData = cinemaAllData?.filter(n => n.slug === slug)[0]

    const allCityData = useSelector(city => city.city.data.data)?.map(city => ({ name: city.name, id: city.id }))
    const cityFilter = allCityData?.filter(n => cityData.includes(n.id))

    useEffect(() => {
        dispatch({
            type: 'FETCH_MOVIES_BY_CINEMA',
            payload: cinemaFilData?.code
        })
        SetCityData(cinemaAllData.map(n => n.cityId))
        SetCineData(cinemaAllData.map(n => ({ name: n.name, slug: n.slug })))

    }, [slug])

    const handleChangeCity = (e) => {
        let cityCine = cinemaAllData.filter(n => n.cityId === e.target.value)
        SetCineData(cityCine.map(n => ({ name: n.name, slug: n.slug })))
    }
    const handleChangeCinema = (e) => {
        nav(`/rap-gia-ve/${e.target.value}`, { replace: true })
    }
    return (
        cinemaAllData && <div className='CineBySlug'>
            <Carousel />
            <div className='CineInfo'>
                <div className='CineInfo-items'>
                    <h1 className='CineInfo_h1'>{cinemaFilData.name}</h1>
                    <p className='CineInfo_p'>Địa chỉ: {cinemaFilData.address}</p>
                    <p className='CineInfo_p'>Hotline: {cinemaFilData.phone} </p>
                </div>
                <div className='option_infor_list'>
                    <select className='option_infor' name="" id="" onChange={(e) => handleChangeCity(e)}>
                        <option>Thành phố</option>
                        {
                            cityFilter?.map((city, i) => {
                                return <option key={i} value={city.id}>
                                    {city.name}
                                </option>
                            })
                        }
                    </select>
                    <select className='option_infor' name="" id="" onChange={(e) => handleChangeCinema(e)}>
                        <option >Rạp</option>
                        {
                            cineData.map((cine, i) => {
                                return <option key={i} value={cine.slug}>
                                    {cine.name}
                                </option>
                            })
                        }
                    </select>
                </div>
            </div>
            <div className='CineBooking'>
                <ShowingByCinema data={cinemaFilData} />
            </div>
        </div>
    )
}
