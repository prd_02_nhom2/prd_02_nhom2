import React from 'react'
import TabFilm from '../../pages/Home/TabFilm/TabFilm'

export default function Film() {
    return (
        <div className='filmpage'>
            <TabFilm />
        </div>
    )
}
